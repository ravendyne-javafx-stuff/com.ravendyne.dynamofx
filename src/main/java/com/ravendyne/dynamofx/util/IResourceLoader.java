package com.ravendyne.dynamofx.util;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import com.ravendyne.dynamofx.cfg.CfgValue;

import javafx.scene.image.Image;
import javafx.scene.text.Font;

public interface IResourceLoader {
    List<String> loadTextResource( String location );
    List<String> loadTextResource( Class<?> clazz, String location );
    List<String> loadTextFile( String location );
    BufferedImage loadImage( String location );
    ImageIcon loadImageIcon( String location );
    Image loadFxImage( String location );
    Font loadFont(String location, int size);
    Map<String, CfgValue> loadConfigFile(String location);
    Map<String, CfgValue> loadConfigFile(List<String> config);
}
