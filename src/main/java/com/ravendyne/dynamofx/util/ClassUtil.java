package com.ravendyne.dynamofx.util;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ClassUtil {
    private static final String SETTER_PREFIX = "set";
    private static final String GETTER_PREFIX = "get";
    
    static final class MethodsMap extends HashMap< String, List< Method > > {
        private static final long serialVersionUID = 7815164329433467750L;
        
        public MethodsMap() {
            super();
        }
    }
    
    private static final Map<Class<?>, MethodsMap> classMethodsCache;
    
    static {
        classMethodsCache = new HashMap<>();
    }

    private ClassUtil() {}

    public static Class< ? > loadClass( String className, String argumentName ) {
        Class< ? > clazz = null;

        try {
            clazz = ClassUtil.class.getClassLoader().loadClass( className );
        } catch ( ClassNotFoundException e ) {
            throw new IllegalArgumentException("Value (" + className + ") is not a valid class name for argument '" + argumentName + "'.", e);
        }

        return clazz;
    }

    //====================================================================
    // http://www.artima.com/weblogs/viewpost.jsp?thread=208860
    /**
     * Get the underlying class for a type, or null if the type is a variable type.
     * @param type the type
     * @return the underlying class
     */
    public static Class< ? > getClass( Type type ) {

        if ( type instanceof Class ) {
            return ( Class< ? > ) type;

        } else if ( type instanceof ParameterizedType ) {
            return getClass( ( ( ParameterizedType ) type ).getRawType() );

        } else if ( type instanceof GenericArrayType ) {
            Type componentType = ( ( GenericArrayType ) type ).getGenericComponentType();
            Class< ? > componentClass = getClass( componentType );

            if ( componentClass != null ) {
                return Array.newInstance( componentClass, 0 ).getClass();
            } else {
                return null;
            }

        } else {
            
            return null;
        }
    }

    /**
     * Get the actual type arguments a child class has used to extend a generic base class.
     *
     * @param baseClass the base class
     * @param childClass the child class
     * @return a list of the raw classes for the actual type arguments.
     */
    public static < T > List< Class< ? > > getTypeArguments( Class< T > baseClass, Class< ? > childClass ) {

        Map< Type, Type > resolvedTypes = new HashMap< Type, Type >();
        Type type = childClass;
        
        // start walking up the inheritance hierarchy until we hit baseClass
        while ( !getClass( type ).equals( baseClass ) ) {
            if ( type instanceof Class ) {

                // there is no useful information for us in raw types, so just keep going.
                type = ( ( Class< ? > ) type ).getGenericSuperclass();

            } else {

                ParameterizedType parameterizedType = ( ParameterizedType ) type;
                Class< ? > rawType = ( Class< ? > ) parameterizedType.getRawType();

                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                TypeVariable< ? >[] typeParameters = rawType.getTypeParameters();
                
                for ( int i = 0; i < actualTypeArguments.length; i++ ) {
                    resolvedTypes.put( typeParameters[ i ], actualTypeArguments[ i ] );
                }

                if ( !rawType.equals( baseClass ) ) {
                    type = rawType.getGenericSuperclass();
                }
            }
        }

        // finally, for each actual type argument provided to baseClass, determine (if
        // possible)
        // the raw class for that type argument.
        Type[] actualTypeArguments;
        if ( type instanceof Class ) {
            actualTypeArguments = ( ( Class< ? > ) type ).getTypeParameters();
        } else {
            actualTypeArguments = ( ( ParameterizedType ) type ).getActualTypeArguments();
        }
        
        List< Class< ? > > typeArgumentsAsClasses = new ArrayList< Class< ? > >();
        
        // resolve types by chasing down type variables.
        for ( Type baseType : actualTypeArguments ) {
            while ( resolvedTypes.containsKey( baseType ) ) {
                baseType = resolvedTypes.get( baseType );
            }
            typeArgumentsAsClasses.add( getClass( baseType ) );
        }
        
        return typeArgumentsAsClasses;
    }

    public static <T> List<Class<?>> getTypeArgumentsWithExtends(Class<T> baseClass, Class<? extends T> childClass) {
        return getTypeArguments( baseClass, childClass );
    }
    //====================================================================

    public static <T> T newInstance( Class< T > aClass, String argumentName ) {
        T anInstance;

        try {
            anInstance = aClass.newInstance();
        } catch ( InstantiationException | IllegalAccessException e ) {
            throw new IllegalArgumentException("Class (" + aClass.getName() + ") must have default constructor publicly accessible for argument '" + argumentName + "'.", e);
        }

        return anInstance;
    }

    
    
    
    public static Method getStaticValueOfMethod( Class< ? > type ) {
        Method method = null;

        List< Method > list = getMethodList( type, "valueOf" );
        for(Method item:list) {
            if(Modifier.isStatic(item.getModifiers())) {
                if(Arrays.equals( item.getParameterTypes(), new Class<?>[] {String.class} ) ||
                        Arrays.equals( item.getParameterTypes(), new Class<?>[] {Object.class} )) {
                    if(item.getReturnType() == type) {
                        method = item;
                    }
                    break;
                }
            }
        }

        return method;
    }
    
    /**
     * Skips static methods.
     * 
     * @param type
     * @param name
     * @param parameterTypes
     * @return
     */
    public static Method getMethod( Class< ? > type, String name, Class<?>[] parameterTypes ) {
        Method method = null;
        
        List< Method > list = getMethodList( type, name );
        for(Method item:list) {
            if(Modifier.isStatic(item.getModifiers())) {
                continue;
            }
            if(Arrays.equals( item.getParameterTypes(), parameterTypes )) {
                method = item;
                break;
            }
        }

        return method;
    }
    
    public static Method getStaticMethod( Class< ? > type, String name, Class<?>[] parameterTypes ) {
        Method method = null;
        
        List< Method > list = getMethodList( type, name );
        for(Method item:list) {
            if(!Modifier.isStatic(item.getModifiers())) {
                continue;
            }
            if(Arrays.equals( item.getParameterTypes(), parameterTypes )) {
                method = item;
                break;
            }
        }

        return method;
    }

    public static List<Method> getMethodList( Class< ? > type, String name ) {
        List< Method > result = new ArrayList<>();
        
        MethodsMap classMethodsMap = classMethodsCache.get( type );
        if(classMethodsMap == null) {
            classMethodsMap = getMethods( type );
        }
        List< Method > methodsList = classMethodsMap.get( name );
        if(methodsList != null) {
            result.addAll( methodsList );
        }
        
        return result;
    }

    public static MethodsMap getMethods( Class< ? > type ) {
        if(classMethodsCache.get( type ) != null) {
            return classMethodsCache.get( type );
        }

        // key is method name, value is a list of methods with the same name,
        // which means a list of all the method overloads
        MethodsMap methodsMap = new MethodsMap();

        // should we do just public? class may be package private...
        if ( Modifier.isPublic( type.getModifiers() ) ) {
            final Method[] declaredMethods = AccessController.doPrivileged(
                    new PrivilegedAction< Method[] >() {
                        @Override
                        public Method[] run() {
                            return type.getDeclaredMethods();
                        }
                    } );
            for ( Method method : declaredMethods ) {
                int methodModifiers = method.getModifiers();
                // only public methods
                if ( Modifier.isPublic( methodModifiers )/* && !Modifier.isStatic( methodModifiers )*/ ) {

                    String methodName = method.getName();
                    List< Method > overridesList = methodsMap.get( methodName );

                    if ( overridesList == null ) {
                        overridesList = new ArrayList<>();
                        methodsMap.put( methodName, overridesList );
                    }

                    overridesList.add( method );
                }

            }

        }

        classMethodsCache.put( type, methodsMap );

        return methodsMap;
    }
    
    static String setterName(String propertyName) {
        return SETTER_PREFIX + toCamelCase( propertyName );
    }
    
    static String getterName(String propertyName) {
        return GETTER_PREFIX + toCamelCase( propertyName );
    }
    
    static String toCamelCase(String name) {
        return Character.toUpperCase( name.charAt( 0 ) ) + name.substring( 1 );
    }
}
