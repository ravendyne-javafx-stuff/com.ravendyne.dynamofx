package com.ravendyne.dynamofx.gui.builders;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

import com.ravendyne.dynamofx.TestResources;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.DynamoFxComponent;

import javafx.scene.layout.Pane;

@ExtendWith(ApplicationExtension.class)
public class BuildersGeneralTest {
    static List<String> guiConfig;

    @Inject Pane myTextField;
    
    @BeforeAll
    public static void init() {
        guiConfig = TestResources.loader.loadConfig(BuildersGeneralTest.class, "general.dynamofx");
    }

    @Test
    public void test1() {
        DynamoFxComponent builder = DynamoFxComponent
            .newInstance("test", guiConfig)
            .setResourceLoader( TestResources.loader )
            .setConfigRoot("sameType")
            .injectInto( this );

        // Will try to assign TextField instance to the Pane field
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            builder.build();
        });
    }

}
