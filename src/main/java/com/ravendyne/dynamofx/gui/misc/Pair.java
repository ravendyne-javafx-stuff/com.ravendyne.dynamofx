package com.ravendyne.dynamofx.gui.misc;

public final class Pair extends ListOfNumbers {

    private Pair() {
    }

    public static Pair valueOf(String value) {
        Pair pair = new Pair();

        String[] parts = value.split(",");

        pair.values.add(null);
        pair.values.add(null);

        if(parts.length > 0) {
            pair.values.add( 0, parseNumber(parts[0]) );
        }
        if(parts.length > 1) {
            pair.values.add( 1, parseNumber(parts[1]) );
        }

        return pair;
    }
    
    public Number getX() {
        return values.get(0);
    }

    public Number getY() {
        return values.get(1);
    }

}
