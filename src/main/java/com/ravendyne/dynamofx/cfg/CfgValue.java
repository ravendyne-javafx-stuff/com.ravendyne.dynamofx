package com.ravendyne.dynamofx.cfg;

import java.util.List;
import java.util.Map;

public interface CfgValue {
//  boolean isNumber();
  boolean isString();
  boolean isMap();
  boolean isList();
  boolean isValueList();
//  Number getNumber();
  String getString();
  Map<String, CfgValue> getMap();
  List<Map<String, CfgValue>> getList();
  List<CfgValue> getValueList();
}
