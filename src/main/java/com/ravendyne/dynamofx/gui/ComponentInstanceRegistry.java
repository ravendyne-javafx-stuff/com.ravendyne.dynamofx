package com.ravendyne.dynamofx.gui;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.ravendyne.dynamofx.gui.types.Inject;

import javafx.scene.layout.Region;

public enum ComponentInstanceRegistry {
    INSTANCE;

    public final class InstanceRegistry extends HashMap<String, Region>{
        private static final long serialVersionUID = 2004615133539824394L;

        public void inject( Object object ) {

            Class< ? > c = object.getClass();
            
            Region component;

            // find fields annotated with @Inject
            for(Field field  : c.getDeclaredFields())
            {
                String fieldName = field.getName();

                if (field.isAnnotationPresent(Inject.class))
                {
                    component = get(fieldName);

                    // skip the field if there is no matching component
                    if(component == null) {
                        // FIXME log skipped field
//                        System.out.println( "Couldn't find component with ID '" + fieldName +"', skipping injection." );
                        continue;
                    }
                    
                    field.setAccessible( true );

                    if( !field.getType().isAssignableFrom( component.getClass() ) ) {
                        throw new IllegalArgumentException(
                                "javafx.scene.layout.Region with ID '" + fieldName +"' is of class '" + component.getClass().getCanonicalName() +
                                "' which can't be injected into the field '" + fieldName +"' of class '" + field.getType().getCanonicalName() + "'." );
                    }

                    try {
                        field.set( object, component );
                    } catch ( IllegalArgumentException e ) {
                        throw e;
                    } catch ( IllegalAccessException e ) {
                        // TODO log the field.set() call error
                        e.printStackTrace();
                    }
                }
            }
            
        }
    }

    Map<String, InstanceRegistry> registries;
    InstanceRegistry currentRegistry;

    private ComponentInstanceRegistry() {
        registries = new HashMap<>();
        currentRegistry = null;
    }
    
    public InstanceRegistry getRegistry(String registryName) {
        ensureRegistryExists( registryName );
        return registries.get( registryName );
    }
    
    private void ensureRegistryExists(String registryName) {
        if(registries.get(registryName) == null) {
            registries.put(registryName, new InstanceRegistry());
        }
    }

    public void setActiveRegistry(String registryName) {
        ensureRegistryExists( registryName );
        currentRegistry = registries.get(registryName);
    }
    
    public void register(String id, Region component) {
        if(currentRegistry != null) {
            currentRegistry.put(id, component);
        }
    }

    public Region get(String id) {
        if(currentRegistry != null) {
            return currentRegistry.get(id);
        }
        return null;
    }

    public void inject( Object object ) {
        if(currentRegistry != null) {
            currentRegistry.inject( object );
        }
    }
}
