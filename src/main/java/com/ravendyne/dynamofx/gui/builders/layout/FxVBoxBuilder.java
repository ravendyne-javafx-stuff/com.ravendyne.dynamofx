package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.geometry.Pos;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public final class FxVBoxBuilder extends RegionBuilder<VBox> {

    public FxVBoxBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxVBoxBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxVBoxBuilder builder = new FxVBoxBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        VBox layout = new VBox();

        /*
         * https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/VBox.html
         * TODO vgrow, margin
         * 
         *      VBox.setVgrow(component, Priority.ALWAYS);
         *      component.setMaxHeight(Double.MAX_VALUE);
         * 
         */
        List< Map< String, CfgValue > > componentConfigs = getList(Attributes.CONTROL_LIST, parameters);

        for(Map< String, CfgValue > config : componentConfigs){

        	Region component = createComponent( config );
            
            // createComponent() would throw an exception if
            // getting attributes wouldn't have worked
            String tag = config.keySet().iterator().next();
            Map<String, CfgValue> attributes = getMap(tag, config);

            applyEnumValue(Priority.class, Attributes.VGROW, attributes, (vGrow) -> {
            	VBox.setVgrow( component, vGrow );
//            	component.setMaxHeight(Double.MAX_VALUE);
            });
            
            applyInsetsValue(Attributes.MARGIN, attributes, (insets) -> {
            	VBox.setMargin(component, insets);
            });

            layout.getChildren().add( component );
        }

        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        applyNumericValue(Attributes.SPACING, parameters, (spacing) -> rootComponent.setSpacing( spacing.doubleValue() ) );

        applyBooleanValue(Attributes.FILL_WIDTH, parameters, (fillWidthValue) -> rootComponent.setFillWidth( fillWidthValue ) );

        applyEnumValue(Pos.class, Attributes.ALIGN, parameters, (alignment) -> rootComponent.setAlignment( alignment ) );
    }

}
