package com.ravendyne.dynamofx.gui.builders.component;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;
import com.ravendyne.dynamofx.util.ClassUtil;

import javafx.scene.control.TreeView;

/**
 *
 * @see <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TreeView.html">TreeView</a>
 */
public class FxTreeViewBuilder extends ComponentBuilder<TreeView<?>> {

    protected FxTreeViewBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxTreeViewBuilder newInstance(Map<String, CfgValue> parameters) {
        FxTreeViewBuilder builder = new FxTreeViewBuilder(parameters);
        return builder;
    }

    private <T> void buildTreeView(Class<T> treeViewTypeClass) {
        TreeView<T> treeView = new TreeView<>();

        rootComponent = idComponent = treeView;
    }

    @Override
    protected void buildComponent() {
        String treeViewType = getStringValue( Attributes.TYPE, "java.lang.String", parameters );

        Class< ? > TreeViewTypeClass = ClassUtil.loadClass( treeViewType, Attributes.TYPE );
        
        buildTreeView( TreeViewTypeClass );
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
