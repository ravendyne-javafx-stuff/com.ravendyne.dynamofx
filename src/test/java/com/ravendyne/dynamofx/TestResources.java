package com.ravendyne.dynamofx;

import java.util.List;

import com.ravendyne.dynamofx.util.BasicResourceLoader;

public final class TestResources extends BasicResourceLoader {
    public static final TestResources loader = new TestResources();
    
    public List<String> loadConfig(Class<?> clazz, String configName) {
        List<String> guiConfig = TestResources.loader.loadTextFile(TestResources.loader.getPackagePath(clazz) + configName);
        return guiConfig;
    }
}
