package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Objects;

import com.ravendyne.dynamofx.TestResources;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.util.SandboxApp;

import javafx.scene.layout.GridPane;

public class GridPaneTestApp extends SandboxApp {
    
    @Inject GridPane gridPane;

    public GridPaneTestApp() {
        configName = TestResources.loader.getPackagePath(getClass()) + "grid_pane_ui.dynamofx";
        title = "GridPane test";
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void run() {
        Objects.requireNonNull(gridPane, "GridPane hasn't been injected");
    }

}
