package com.ravendyne.dynamofx.gui.builders.component.textinput;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentTextInputControlBuilder;

import javafx.scene.control.TextArea;

public final class FxTextAreaBuilder extends ComponentTextInputControlBuilder<TextArea> {

    private FxTextAreaBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxTextAreaBuilder newInstance(Map<String, CfgValue> parameters) {
        FxTextAreaBuilder builder = new FxTextAreaBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        TextArea component = new TextArea();

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        applyNumericValue(Attributes.ROWS, parameters, (rows) -> rootComponent.setPrefRowCount( rows.intValue() ) );

        applyNumericValue(Attributes.COLS, parameters, (columns) -> rootComponent.setPrefColumnCount( columns.intValue() ) );

        applyBooleanValue(Attributes.WRAP, parameters, (wrap) -> rootComponent.setWrapText( wrap ) );
    }

}
