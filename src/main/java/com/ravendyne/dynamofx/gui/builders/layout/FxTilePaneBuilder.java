package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.layout.TilePane;

public final class FxTilePaneBuilder extends RegionBuilder<TilePane> {

    public FxTilePaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxTilePaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxTilePaneBuilder builder = new FxTilePaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        TilePane layout = new TilePane();

        // TODO set individual child alignment/margin
        /*
         * i.e.
         * StackPane.setAlignment(title, Pos.BOTTOM_CENTER);
         * StackPane.setMargin(list, new Insets(8,8,8,8);
         * 
         * list = getList(Attributes.CONTROL_LIST, parameters)
         * for item in list
         *      createComponent(item)
         *      align = item.getTileAlign (i.e. Pos.CENTER_LEFT)
         *      c = createComponent(item)
         *      c.setTileAlignment(align);
         *      layout.getChildren().add(c)
         */
        layout.getChildren().addAll(createComponents(getList(Attributes.CONTROL_LIST, parameters)));

        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        // TODO other properties
//        IntegerProperty     prefColumns
//        IntegerProperty     prefRows
//        DoubleProperty     prefTileHeight
//        DoubleProperty     prefTileWidth
//        ObjectProperty<Pos>     tileAlignment
//        ReadOnlyDoubleProperty     tileHeight
//        ReadOnlyDoubleProperty     tileWidth

        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/TilePane.html

        applyEnumValue(Pos.class, Attributes.ALIGN, parameters, (alignment) -> rootComponent.setAlignment( alignment ) );

        applyEnumValue(Orientation.class, Attributes.ORIENTATION, parameters, (orientation) -> rootComponent.setOrientation( orientation ) );

        applyNumericValue(Attributes.HGAP, parameters, (hGap) -> rootComponent.setHgap( hGap.doubleValue() ) );

        applyNumericValue(Attributes.VGAP, parameters, (vGap) -> rootComponent.setVgap( vGap.doubleValue() ) );
    }

}
