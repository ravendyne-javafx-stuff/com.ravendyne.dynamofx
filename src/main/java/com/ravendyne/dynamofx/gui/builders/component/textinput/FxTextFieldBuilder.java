package com.ravendyne.dynamofx.gui.builders.component.textinput;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentTextInputControlBuilder;

import javafx.scene.control.TextField;

/**
 * @see <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TextField.html">JavaFX TextField</a>
 */
public final class FxTextFieldBuilder extends ComponentTextInputControlBuilder<TextField> {

    private FxTextFieldBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxTextFieldBuilder newInstance(Map<String, CfgValue> parameters) {
        FxTextFieldBuilder builder = new FxTextFieldBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        TextField component = new TextField();

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        applyNumericValue(Attributes.COLS, parameters, (cols) -> rootComponent.setPrefColumnCount( cols.intValue() ) );
    }

}
