package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

public final class FxGridPaneBuilder extends RegionBuilder<GridPane> {

    public FxGridPaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxGridPaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxGridPaneBuilder builder = new FxGridPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        GridPane layout = new GridPane();

        List< Map< String, CfgValue > > componentConfigs = getList(Attributes.CONTROL_LIST, parameters);

        for(Map< String, CfgValue > config : componentConfigs){

        	Region component = createComponent( config );
            
            // createComponent() would throw an exception if
            // getting attributes wouldn't have worked
            String tag = config.keySet().iterator().next();
            Map<String, CfgValue> attributes = getMap(tag, config);

            applyNumericValue(Attributes.ROW_INDEX, attributes, (rowIndex) -> {
            	GridPane.setRowIndex( component, rowIndex.intValue() );
            });

            applyNumericValue(Attributes.COLUMN_INDEX, attributes, (columnIndex) -> {
            	GridPane.setColumnIndex( component, columnIndex.intValue() );
            });

            applyEnumValue(HPos.class, Attributes.H_ALIGNMENT, attributes, (hAlign) -> {
            	GridPane.setHalignment( component, hAlign );
            });

            applyEnumValue(VPos.class, Attributes.V_ALIGNMENT, attributes, (vAlign) -> {
            	GridPane.setValignment( component, vAlign );
            });

            layout.getChildren().add( component );
        }

        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/GridPane.html

        applyEnumValue(Pos.class, Attributes.ALIGN, parameters, (alignment) -> rootComponent.setAlignment( alignment ) );

        applyNumericValue(Attributes.HGAP, parameters, (hGap) -> rootComponent.setHgap( hGap.doubleValue() ) );

        applyNumericValue(Attributes.VGAP, parameters, (vGap) -> rootComponent.setVgap( vGap.doubleValue() ) );
    }

}
