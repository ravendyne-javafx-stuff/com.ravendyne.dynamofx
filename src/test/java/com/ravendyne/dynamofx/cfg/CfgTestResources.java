package com.ravendyne.dynamofx.cfg;

import java.util.List;

import com.ravendyne.dynamofx.util.BasicResourceLoader;
import com.ravendyne.dynamofx.cfg.mouseruntime.SourceString;

public final class CfgTestResources extends BasicResourceLoader {
    private CfgTestResources() {}

    public static final CfgTestResources loader = new CfgTestResources();

    public SourceString loadTestConfig(String location) {
        List< String > config = CfgTestResources.loader.loadTextFile( location );
        String lineSeparator = System.getProperty("line.separator", "\n");
        SourceString source = new SourceString( String.join( lineSeparator, config ) );
        
        return source;
    }
}
