package com.ravendyne.dynamofx.gui.builders.component;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;

import javafx.scene.control.ScrollPane;

public final class FxScrollPaneBuilder extends ComponentBuilder<ScrollPane> {

    private FxScrollPaneBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxScrollPaneBuilder newInstance(Map<String, CfgValue> parameters) {
        FxScrollPaneBuilder builder = new FxScrollPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        ScrollPane component = new ScrollPane();

        component.setContent(createComponent(getMap(Attributes.CONTENT, parameters)));

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
