package com.ravendyne.dynamofx.gui.builders.component.labeled;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentLabeledBuilder;

import javafx.scene.control.TitledPane;
import javafx.scene.layout.Region;

public final class FxTitledPaneBuilder extends ComponentLabeledBuilder<TitledPane> {

    private FxTitledPaneBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxTitledPaneBuilder newInstance(Map<String, CfgValue> parameters) {
        FxTitledPaneBuilder builder = new FxTitledPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        TitledPane component = new TitledPane();

        Region content = createComponent(getMap(Attributes.CONTENT, parameters));
        component.setContent(content);

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
        
        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TitledPane.html
        
        applyBooleanValue(Attributes.ANIMATED, parameters, (animated) -> rootComponent.setAnimated( animated ) );
        
        applyBooleanValue(Attributes.COLLAPSIBLE, parameters, (collapsible) -> rootComponent.setCollapsible( collapsible ) );
        
        applyBooleanValue(Attributes.EXAPNDED, parameters, (expanded) -> rootComponent.setExpanded( expanded ) );
    }

}
