package com.ravendyne.dynamofx.sandbox;

import java.util.Objects;

import com.ravendyne.dynamofx.TestResources;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.util.SandboxApp;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

public class TreeViewSandbox extends SandboxApp {
    
    @Inject TreeView<String> treeView;

    public TreeViewSandbox() {
        configName = TestResources.loader.getPackagePath(getClass()) + "tree_view.dynamofx";
        title = "TreeView test";
    }

    public static void main(String[] args) {
        launch(args);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void run() {
        Objects.requireNonNull(treeView, "TreeView hasn't been injected");

        TreeItem<String> root = new TreeItem<String>("Root");
        root.setExpanded(true);

        TreeItem<String> bono = new TreeItem<String>("bono");
        bono.getChildren().addAll(
                new TreeItem<String>("bono 1"),
                new TreeItem<String>("bono 2"),
                new TreeItem<String>("bono 3")
            );

        root.getChildren().addAll(
            new TreeItem<String>("Item 1"),
            bono,
            new TreeItem<String>("Item 2"),
            new TreeItem<String>("Item 3")
        );

        treeView.setRoot(root);

    }

}
