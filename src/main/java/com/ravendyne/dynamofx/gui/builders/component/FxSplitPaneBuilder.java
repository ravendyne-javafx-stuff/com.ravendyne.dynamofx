package com.ravendyne.dynamofx.gui.builders.component;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;
import com.ravendyne.dynamofx.gui.misc.NumberList;

import javafx.geometry.Orientation;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Region;

public final class FxSplitPaneBuilder extends ComponentBuilder<SplitPane> {

    private FxSplitPaneBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxSplitPaneBuilder newInstance(Map<String, CfgValue> parameters) {
        FxSplitPaneBuilder builder = new FxSplitPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        SplitPane component = new SplitPane();

        List< Map< String, CfgValue > > componentConfigs = getList(Attributes.CONTROL_LIST, parameters);
        for(Map< String, CfgValue > config : componentConfigs){

            Region childComponent = createComponent( config );

            // createComponent() would throw an exception if
            // getting attributes wouldn't have worked
            String tag = config.keySet().iterator().next();
            Map<String, CfgValue> attributes = getMap(tag, config);


            applyBooleanValue(Attributes.RESIZABLE_WITH_PARENT, attributes, (resizable) -> {
                SplitPane.setResizableWithParent(childComponent, resizable);
            });


            component.getItems().add( childComponent );

        }

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
        
        applyStringValue(Attributes.DIVIDERS, parameters, (dividersSpec) -> {
            NumberList numberList = NumberList.valueOf(dividersSpec);
            rootComponent.setDividerPositions(numberList.asDoubleArray());
        });

        applyEnumValue(Orientation.class, Attributes.ORIENTATION, parameters, (orientation) -> rootComponent.setOrientation( orientation ) );
    }

}
