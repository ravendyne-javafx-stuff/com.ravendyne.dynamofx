package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;

public final class FxStackPaneBuilder extends RegionBuilder<StackPane> {

    public FxStackPaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxStackPaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxStackPaneBuilder builder = new FxStackPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        StackPane layout = new StackPane();

        // TODO set individual child alignment/margin
        /*
         * i.e.
         * StackPane.setAlignment(title, Pos.BOTTOM_CENTER);
         * StackPane.setMargin(list, new Insets(8,8,8,8);
         * 
         */
        layout.getChildren().addAll(createComponents(getList(Attributes.CONTROL_LIST, parameters)));

        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/StackPane.html

        applyEnumValue(Pos.class, Attributes.ALIGN, parameters, (alignment) -> rootComponent.setAlignment( alignment ) );
    }

}
