package com.ravendyne.dynamofx.gui.types;

public interface IControlBuilder<T> {
    T getControl();
    void build();
}
