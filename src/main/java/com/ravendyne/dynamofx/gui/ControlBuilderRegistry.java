package com.ravendyne.dynamofx.gui;

import java.util.HashMap;
import java.util.Map;

import com.ravendyne.dynamofx.DynamoFxResources;
import com.ravendyne.dynamofx.gui.types.IControlBuilderFactory;
import com.ravendyne.dynamofx.util.IResourceLoader;

public enum ControlBuilderRegistry {
    INSTANCE;

    private final Map<String, IControlBuilderFactory> registrar;
    private IResourceLoader resourceLoader;

    ControlBuilderRegistry() {
        registrar = new HashMap<>();
        registerDefaults();
        resourceLoader = DynamoFxResources.loader;
    }
    
    private void registerDefaults() {
        for( String tag : Builders.controls.keySet() ) {
            register(tag, Builders.controls.get(tag));
        }
    }

    public IControlBuilderFactory getFactoryFor(String tag) {
        return registrar.get(tag);
    }
    
    public void register(String tag, IControlBuilderFactory builder) {
        registrar.put(tag, builder);
    }
    
    public void setResourceLoader(IResourceLoader loader) {
        resourceLoader = loader;
    }

    public IResourceLoader getResourceLoader() {
        return resourceLoader;
    }
}
