package com.ravendyne.dynamofx.cfg;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.mouseruntime.SourceString;

public class Cfg {
    
    private String errorMessage;
    private Map<String, CfgValue> result;

    public Cfg(String source) {
        CfgParser parser = new CfgParser();
        SourceString src = new SourceString(source);
        
        boolean ok = parser.parse(src);
        CfgSemantics sem = parser.semantics();
        if (ok)
        {
            errorMessage = "";
            result = sem.getCfg();
        } else {
            errorMessage = sem.getErrorMessage();
        }
    }
    
    public Map<String, CfgValue> get() {
        return result;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

}
