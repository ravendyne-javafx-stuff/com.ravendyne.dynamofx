package com.ravendyne.dynamofx.cfg;

import java.io.PrintStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CfgBuilder {
    public static final String GRAPH_KEY = "graph";

    public static String indent = "  ";
    
    static class ValueBuilderBase implements CfgValueBuilder {

        @Override
        public boolean isString() {
            return false;
        }

        @Override
        public boolean isMap() {
            return false;
        }

        @Override
        public boolean isList() {
            return false;
        }

        @Override
        public boolean isValueList() {
            return false;
        }

        @Override
        public String getString() {
            return null;
        }

        @Override
        public Map<String, CfgValue> getMap() {
            return null;
        }

        @Override
        public List<CfgValue> getValueList() {
            return null;
        }

        @Override
        public List<Map<String, CfgValue>> getList() {
            return null;
        }

        @Override
        public void setString(String value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setMapItem(String key, CfgValue value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addListItem(CfgValue value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addAllListItems(CfgValue value) {
            throw new UnsupportedOperationException();
        }
        
    }

    static class StringValue extends ValueBuilderBase {
        private String value;
        
        public StringValue() {
            value = null;
        }

        @Override
        public boolean isString() {
            return true;
        }

        @Override
        public String getString() {
            return value;
        }

        @Override
        public void setString(String value) {
            this.value = value;
        }
    }

    static class MapValue extends ValueBuilderBase {
        private Map<String, CfgValue> value;
        
        public MapValue() {
            value = new HashMap<>();
        }

        @Override
        public boolean isMap() {
            return true;
        }

        @Override
        public Map<String, CfgValue> getMap() {
            return value;
        }

        @Override
        public void setMapItem(String key, CfgValue value) {
            this.value.put(key, value);
        }
    }

    static class ListValue extends ValueBuilderBase {
        private List<CfgValue> value;
        
        public ListValue() {
            value = new ArrayList<>();
        }

        @Override
        public boolean isList() {
            return true;
        }

        @Override
        public List<CfgValue> getValueList() {
            return value;
        }

        @Override
        public void addListItem(CfgValue value) {
            this.value.add(value);
        }

        @Override
        public void addAllListItems(CfgValue value) {
            if(! value.isList()) {
                throw new IllegalArgumentException("Can only add CfgValue that is a list");
            }
            
//            this.value.addAll(value.getList());
        }
    }

    public static CfgValueBuilder newStringValue() {
        return new StringValue();
    }

    public static CfgValueBuilder newStringValue(String value) {
        final StringValue stringValue = new StringValue();
        stringValue.setString(value);
        return stringValue;
    }

    public static CfgValueBuilder newMapValue() {
        return new MapValue();
    }

    public static CfgValueBuilder newListValue() {
        return new ListValue();
    }

    public static void save(CfgValue graphMap, PrintStream out) {
        saveMap(GRAPH_KEY, graphMap.getMap(), out, 0);
    }

    public static void save(String rootKey, CfgValue graphMap, PrintStream out) {
        if(! graphMap.isMap()) {
            throw new InvalidParameterException("Can only save as a root CfgValue which is a map.");
        }

        saveMap(rootKey, graphMap.getMap(), out, 0);
    }

    public static String indent(int level) {
        return String.join("", Collections.nCopies(level, indent));
    }
    
    private static void saveString(String key, String value, PrintStream out, int indentLevel) {
        out.print(indent(indentLevel));
        out.print(key + " ");
        out.println("\"" + value.replace('"', '\'') + "\"");
    }

    private static void saveMap(String mapKey, Map<String, CfgValue> map, PrintStream out, int indentLevel) {
        String indent = indent(indentLevel);

        out.print(indent);
        out.println(mapKey + " [");

        for(String key : map.keySet()) {
            CfgValue item = map.get(key);

            if(item.isString()) {
                saveString(key, item.getString(), out, indentLevel + 1);
            } else if(item.isMap()) {
                saveMap(key, item.getMap(), out, indentLevel + 1);
            } else if(item.isList()) {
//                saveValueList(key, item.getList(), out, indentLevel + 1);
            }
        }
        
        out.print(indent);
        out.println("]");
        
    }
    
    private static void saveList(String key, List<CfgValue> list, PrintStream out, int indentLevel) {
        for(CfgValue item : list) {
            if(item.isMap()) {
                Map<String, CfgValue> map = item.getMap();
                saveMap(key, map, out, indentLevel);
            }
        }
    }

}
