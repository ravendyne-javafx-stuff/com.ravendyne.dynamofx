package com.ravendyne.dynamofx.cfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

abstract class Node implements CfgValue {

    // @Override
    // public boolean isNumber() {
    // return false;
    // }

    @Override
    public boolean isString() {
        return false;
    }

    @Override
    public boolean isMap() {
        return true;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isValueList() {
        return false;
    }

    // @Override
    // public Number getNumber() {
    // return null;
    // }

    @Override
    public String getString() {
        return null;
    }

    @Override
    public Map<String, CfgValue> getMap() {
        return null;
    }

    @Override
    public List<Map<String, CfgValue>> getList() {
        return null;
    }

    @Override
    public List<CfgValue> getValueList() {
        return null;
    }

    static final class Key {
        public String name;

        public Key(String name) {
            Objects.requireNonNull(name);
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    static final class KeyValuePair {
        public Key key;
        public CfgValue value;

        public KeyValuePair(Key key, CfgValue value) {
            Objects.requireNonNull(key);
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return key + "->" + value;
        }
    }

    // static final class Numeric extends Node {
    // Number value;
    //
    // public Numeric(Object value) {
    // if(value instanceof Integer || value instanceof Double) {
    // this.value = (Number)value;
    // } else if(value instanceof String) {
    // this.value = Double.valueOf((String)value);
    // }
    // }
    //
    // @Override
    // public String toString() {
    // return value.toString();
    // }
    //
    // @Override
    // public boolean isNumber() {
    // return true;
    // }
    //
    // @Override
    // public Number getNumber() {
    // return value;
    // }
    //
    // }

    static final class Text extends Node {
        String value;

        public Text(Object value) {
            this.value = String.valueOf(value);
        }

        @Override
        public String toString() {
            return "'''" + value + "'''";
        }

        @Override
        public boolean isString() {
            return true;
        }

        @Override
        public String getString() {
            return value;
        }
    }

    static final class KeyValueMap extends Node {
        Map<String, CfgValue> map;

        public KeyValueMap() {
            map = new HashMap<>();
        }

        public void add(KeyValuePair pair) {
            map.put(pair.key.name, pair.value);
        }

        @Override
        public String toString() {
            return map.toString();
        }

        @Override
        public boolean isMap() {
            return true;
        }

        @Override
        public Map<String, CfgValue> getMap() {
            return map;
        }
    }

    static final class MapList extends Node {
        List<Map<String, CfgValue>> list;

        public MapList() {
            list = new ArrayList<>();
        }

        public void add(Map<String, CfgValue> map) {
            list.add(map);
        }

        @Override
        public String toString() {
            return list.toString();
        }

        @Override
        public boolean isList() {
            return true;
        }

        @Override
        public List<Map<String, CfgValue>> getList() {
            return list;
        }
    }

    static final class ValueList extends Node {
        List<CfgValue> list;

        public ValueList() {
            list = new ArrayList<>();
        }

        public void add(CfgValue value) {
            list.add(value);
        }

        @Override
        public String toString() {
            return list.toString();
        }

        @Override
        public boolean isValueList() {
            return true;
        }

        @Override
        public List<CfgValue> getValueList() {
            return list;
        }
    }
}
