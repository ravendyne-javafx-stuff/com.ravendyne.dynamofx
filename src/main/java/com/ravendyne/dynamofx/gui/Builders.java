package com.ravendyne.dynamofx.gui;

import java.util.Map;

import com.ravendyne.dynamofx.gui.builders.component.FxAccordionBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxChoiceBoxBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxListViewBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxProgressBarBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxScrollPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxSliderBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxSplitPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxTabPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.component.FxTreeViewBuilder;
import com.ravendyne.dynamofx.gui.builders.component.labeled.FxButtonBuilder;
import com.ravendyne.dynamofx.gui.builders.component.labeled.FxHyperlinkBuilder;
import com.ravendyne.dynamofx.gui.builders.component.labeled.FxLabelBuilder;
import com.ravendyne.dynamofx.gui.builders.component.labeled.FxTitledPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.component.textinput.FxPasswordFieldBuilder;
import com.ravendyne.dynamofx.gui.builders.component.textinput.FxTextAreaBuilder;
import com.ravendyne.dynamofx.gui.builders.component.textinput.FxTextFieldBuilder;
import com.ravendyne.dynamofx.gui.builders.control.FxTabBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxAnchorPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxBorderPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxFlowPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxGridPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxHBoxBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxStackPaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxTilePaneBuilder;
import com.ravendyne.dynamofx.gui.builders.layout.FxVBoxBuilder;
import com.ravendyne.dynamofx.gui.types.IComponentBuilderFactory;
import com.ravendyne.dynamofx.gui.types.IControlBuilderFactory;
import com.ravendyne.dynamofx.util.MapBuilder;

final class Builders {
    private Builders() {}

    //--------------------------------------------------------------------------
    // CONTROLS
    //--------------------------------------------------------------------------
    public static final Map<String, IControlBuilderFactory> controls = 
        MapBuilder.<String, IControlBuilderFactory>newInstance()

        //--------------------------------------------------------------------------

        // Tab
        .add(Tags.TAB, FxTabBuilder::newInstance)

        //--------------------------------------------------------------------------

        // convert all to unmodifiable map
        .build();

    //--------------------------------------------------------------------------
    // COMPONENTS
    //--------------------------------------------------------------------------
    public static final Map<String, IComponentBuilderFactory> components = 
        MapBuilder.<String, IComponentBuilderFactory>newInstance()
        // TODO make tag names static final Strings somewhere public

        //--------------------------------------------------------------------------

        // Region
        .add(Tags.PANE, FxPaneBuilder::newInstance)

        // Region -> Pane
        .add(Tags.ANCHOR_PANE, FxAnchorPaneBuilder::newInstance)
        .add(Tags.BORDER_PANE, FxBorderPaneBuilder::newInstance)
        .add(Tags.FLOW_PANE, FxFlowPaneBuilder::newInstance)
        .add(Tags.GRID_PANE, FxGridPaneBuilder::newInstance)
        .add(Tags.HBOX, FxHBoxBuilder::newInstance)
        .add(Tags.STACK_PANE, FxStackPaneBuilder::newInstance)
        .add(Tags.TILE_PANE, FxTilePaneBuilder::newInstance)
        .add(Tags.VBOX, FxVBoxBuilder::newInstance)

        // Region -> Pane : special controls
        // TODO DialogPane (to be used as root of javafx.scene.control.Dialog<R>)
        // TODO TextFlow (to be used to layout one or more Text instances)

        // Region -> Control
        // Accordion https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/Accordion.html
        .add(Tags.ACCORDION, FxAccordionBuilder::newInstance)
        // TODO ButtonBar https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ButtonBar.html
        // ChoiceBox
        .add(Tags.CHOICE_BOX, FxChoiceBoxBuilder::newInstance)
        // TODO ComboBoxBase
            // TODO ColorPicker
            // TODO ComboBox
            // TODO DatePicker
        // TODO HTMLEditor
        // ListView https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ListView.html
        .add(Tags.LIST_VIEW, FxListViewBuilder::newInstance)
        // TODO MenuBar
        // TODO Pagination
        // TODO ProgressIndicator
            // TODO ProgressBar
        .add(Tags.PROGRESS_BAR, FxProgressBarBuilder::newInstance)
        // TODO ScrollBar
        // ScrollPane https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ScrollPane.html
        .add(Tags.SCROLL_PANE, FxScrollPaneBuilder::newInstance)
        // TODO Separator
        // Slider
        .add(Tags.SLIDER, FxSliderBuilder::newInstance)
        // TODO Spinner
        // SplitPane https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/SplitPane.html
        .add(Tags.SPLIT_PANE, FxSplitPaneBuilder::newInstance)
        // TODO TableView
        // TODO TabPane https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TabPane.html
        .add(Tags.TAB_PANE, FxTabPaneBuilder::newInstance)
        // TextInputControl
            // TextArea
        .add(Tags.TEXT_AREA, FxTextAreaBuilder::newInstance)
            // TextField
        .add(Tags.TEXT_FIELD, FxTextFieldBuilder::newInstance)
                // PasswordField
        .add(Tags.PASSWORD_FIELD, FxPasswordFieldBuilder::newInstance)
        // TODO ToolBar
        // TODO TreeTableView
        // TreeView
        .add(Tags.TREE_VIEW, FxTreeViewBuilder::newInstance)

        // Region -> Control -> Labeled
        .add(Tags.LABEL, FxLabelBuilder::newInstance)
        // TitledPane https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TitledPane.html
        .add(Tags.TITLED_PANE, FxTitledPaneBuilder::newInstance)

        // Region -> Control -> Labeled -> ButtonBase
        .add(Tags.BUTTON, FxButtonBuilder::newInstance)
        .add(Tags.HYPERLINK, FxHyperlinkBuilder::newInstance)

        //--------------------------------------------------------------------------

        // convert all to unmodifiable map
        .build();

}
