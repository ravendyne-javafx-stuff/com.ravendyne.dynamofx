package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.layout.FlowPane;

public final class FxFlowPaneBuilder extends RegionBuilder<FlowPane> {

    public FxFlowPaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxFlowPaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxFlowPaneBuilder builder = new FxFlowPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        FlowPane layout = new FlowPane();

        layout.getChildren().addAll(createComponents(getList(Attributes.CONTROL_LIST, parameters)));

        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        // TODO: columnHalignment, rowValignment
        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/FlowPane.html

        applyEnumValue(Pos.class, Attributes.ALIGN, parameters, (align) -> rootComponent.setAlignment(align) );

        applyNumericValue(Attributes.HGAP, parameters, (hGap) -> rootComponent.setHgap( hGap.doubleValue() ) );

        applyNumericValue(Attributes.VGAP, parameters, (vGap) -> rootComponent.setVgap( vGap.doubleValue() ) );

        applyNumericValue(Attributes.WRAP, parameters, (wrapLength) -> rootComponent.setPrefWrapLength( wrapLength.doubleValue() ) );

        applyEnumValue(Orientation.class, Attributes.ORIENTATION, parameters, (orientation) -> rootComponent.setOrientation(orientation) );
    }

}
