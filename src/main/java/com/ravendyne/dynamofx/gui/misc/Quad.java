package com.ravendyne.dynamofx.gui.misc;

import java.awt.Insets;

public final class Quad extends ListOfNumbers {

    private Quad() {
    }
    
    public Insets asInsets() {
        // top, left, bottom, right
        return new Insets( getX().intValue(), getY().intValue(), getZ().intValue(), getW().intValue() );
    }

    public static Quad valueOf(String value) {
        Quad quad = new Quad();
        
        if(value == null) {
            return null;
        }

        quad.values.add(null);
        quad.values.add(null);
        quad.values.add(null);
        quad.values.add(null);

        String[] parts = value.split(",");

        if(parts.length > 0) {
            quad.values.add( 0, parseNumber(parts[0]) );
        }
        if(parts.length > 1) {
            quad.values.add( 1, parseNumber(parts[1]) );
        }
        if(parts.length > 2) {
            quad.values.add( 2, parseNumber(parts[2]) );
        }
        if(parts.length > 3) {
            quad.values.add( 3, parseNumber(parts[3]) );
        }

        return quad;
    }

    public Number getX() {
        return values.get( 0 );
    }

    public Number getY() {
        return values.get( 1 );
    }

    public Number getZ() {
        return values.get( 2 );
    }

    public Number getW() {
        return values.get( 3 );
    }
}
