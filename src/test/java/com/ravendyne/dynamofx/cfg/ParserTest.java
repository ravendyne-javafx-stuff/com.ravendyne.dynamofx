package com.ravendyne.dynamofx.cfg;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.ravendyne.dynamofx.cfg.mouseruntime.SourceString;

public class ParserTest {

    @Disabled
    public void blah() {
    }

    @DisplayName("basic characteristics of  parser/semantics objects")
    @Test
    public void basics() {
        SourceString config;
        Map<String, CfgValue> map;

        CfgParser parser = new CfgParser();

        config = new SourceString("");
        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());
        Assertions.assertNotNull(parser.semantics().getCfg());
        Assertions.assertTrue(parser.semantics().getCfg().isEmpty());

        config = new SourceString("key {}");
        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());
        Assertions.assertNotNull(parser.semantics().getCfg());
        Assertions.assertFalse(parser.semantics().getCfg().isEmpty());
        Assertions.assertTrue(parser.semantics().getCfg().containsKey("key"));
        Assertions.assertTrue(parser.semantics().getCfg().get("key").isMap());
        Assertions.assertTrue(parser.semantics().getCfg().get("key").getMap().isEmpty());

        config = new SourceString("key { value:   value line     }");
        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());
        map = parser.semantics().getCfg().get("key").getMap();
        Assertions.assertFalse(map.isEmpty());
        Assertions.assertTrue(map.containsKey("value"));
        Assertions.assertTrue(map.get("value").isString());
        Assertions.assertEquals("   value line     ", map.get("value").getString());

        config = new SourceString("key { list < listValue01 {} listValue02 {} > }");
        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());
        map = parser.semantics().getCfg().get("key").getMap();
        Assertions.assertTrue(map.get("list").isList());
        List<Map<String, CfgValue>> list = map.get("list").getList();
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertEquals(2, list.size());

        config = new SourceString("key { map { listValue01: value1 \n listValue02 : value2 } }");
        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());
        map = parser.semantics().getCfg().get("key").getMap();
        Assertions.assertTrue(map.get("map").isMap());
        map = map.get("map").getMap();
        Assertions.assertFalse(map.isEmpty());
        Assertions.assertEquals(2, map.size());
    }

    @DisplayName("empty top level map")
    @Test
    public void testComment() {
        SourceString config = CfgTestResources.loader.loadTestConfig("01.dynamofx");

        CfgParser parser = new CfgParser();

        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());

        Map<String, CfgValue> cfg = parser.semantics().getCfg();

        Assertions.assertNotNull(cfg);
    }

    @DisplayName("comments ignored")
    @Test
    public void test01() {
        SourceString config = CfgTestResources.loader.loadTestConfig("comments.dynamofx");

        CfgParser parser = new CfgParser();

        Assertions.assertTrue(parser.parse(config));
        Assertions.assertTrue(parser.semantics().getErrorMessage().isEmpty());

        Assertions.assertFalse(parser.semantics().getCfg().isEmpty());
        Assertions.assertTrue(parser.semantics().getCfg().containsKey("comments"));
        Assertions.assertFalse(parser.semantics().getCfg().get("comments").getMap().isEmpty());

        Map<String, CfgValue> map = parser.semantics().getCfg().get("comments").getMap();
        Assertions.assertNotNull(map.get("key"));
        Assertions.assertTrue(map.get("key").isString());
        Assertions.assertEquals(" value ", map.get("key").getString());
        
        Assertions.assertNotNull(map.get("maplist"));
        Assertions.assertTrue(map.get("maplist").isList());
        Assertions.assertEquals(1, map.get("maplist").getList().size());
        
        Assertions.assertNotNull(map.get("valuelist"));
        Assertions.assertTrue(map.get("maplist").isList());
        Assertions.assertTrue(map.get("valuelist").isValueList());
        Assertions.assertEquals(1, map.get("valuelist").getValueList().size());
        Assertions.assertEquals(" valuelistvalue ", map.get("valuelist").getValueList().get(0).getString());
    }
}
