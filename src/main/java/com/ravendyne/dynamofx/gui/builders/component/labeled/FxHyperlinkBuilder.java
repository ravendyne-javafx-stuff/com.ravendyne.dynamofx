package com.ravendyne.dynamofx.gui.builders.component.labeled;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.builders.ComponentLabeledBuilder;

import javafx.scene.control.Hyperlink;

public final class FxHyperlinkBuilder extends ComponentLabeledBuilder<Hyperlink> {

    private FxHyperlinkBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxHyperlinkBuilder newInstance(Map<String, CfgValue> parameters) {
        FxHyperlinkBuilder builder = new FxHyperlinkBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        Hyperlink hyperlink = new Hyperlink();

        rootComponent = idComponent = hyperlink;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
