package com.ravendyne.dynamofx.gui.builders.component;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.Tags;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;

import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.Region;

public final class FxAccordionBuilder extends ComponentBuilder<Accordion> {

    private FxAccordionBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxAccordionBuilder newInstance(Map<String, CfgValue> parameters) {
        FxAccordionBuilder builder = new FxAccordionBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        Accordion component = new Accordion();

        List<Map<String, CfgValue>> panesListParameters = getList(Attributes.PANES, parameters);
        for(Map<String, CfgValue> itemParameters : panesListParameters) {
            if(!itemParameters.isEmpty()) {
                String tag = itemParameters.keySet().iterator().next();
                if(!tag.equals( Tags.TITLED_PANE )) {
                    throw new IllegalArgumentException( Tags.ACCORDION+" '"+Attributes.PANES+"' attribute can only contain elements of type '"+Tags.TITLED_PANE+"'. Invalid element tag was '" + tag + "'" );
                }

                Region titledPane = createComponent(itemParameters);
                component.getPanes().add((TitledPane) titledPane);
            }
        }

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
