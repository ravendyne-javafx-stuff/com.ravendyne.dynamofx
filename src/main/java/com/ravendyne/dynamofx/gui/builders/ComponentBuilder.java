package com.ravendyne.dynamofx.gui.builders;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;

import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;

//javafx.scene.layout.Region -> javafx.scene.control.Control
public abstract class ComponentBuilder<T extends Control> extends RegionBuilder<T> {

    protected ComponentBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    @Override
    protected void setParameters(T controlLabeled) {
        super.setParameters( controlLabeled );

        applyStringValue(Attributes.TOOLTIP, parameters, (tooltip) -> rootComponent.setTooltip( new Tooltip(tooltip) ) );
    }

}
