package com.ravendyne.dynamofx.gui.builders;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.types.IControlBuilder;

//javafx.scene.layout.Region -> javafx.scene.control.Control
public abstract class ControlBuilderBase<T> extends Builder implements IControlBuilder<T> {

    protected T controlInstance;

    protected ControlBuilderBase(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    @Override
    public T getControl() {
        return controlInstance;
    }
    
    @Override
    public void build() {
        buildControl();
        setControlParameters();
    }
    
    protected void setParameters(T parent) {};
    
    protected abstract void buildControl();
    protected abstract void setControlParameters();

}
