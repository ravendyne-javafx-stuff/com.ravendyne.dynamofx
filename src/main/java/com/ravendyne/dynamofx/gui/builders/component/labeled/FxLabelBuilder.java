package com.ravendyne.dynamofx.gui.builders.component.labeled;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.builders.ComponentLabeledBuilder;

import javafx.scene.control.Label;

public final class FxLabelBuilder extends ComponentLabeledBuilder<Label> {

    private FxLabelBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxLabelBuilder newInstance(Map<String, CfgValue> parameters) {
        FxLabelBuilder builder = new FxLabelBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        Label label = new Label();

        rootComponent = idComponent = label;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
