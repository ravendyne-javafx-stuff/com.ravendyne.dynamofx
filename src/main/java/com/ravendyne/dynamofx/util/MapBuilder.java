package com.ravendyne.dynamofx.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class MapBuilder<K, V> {
    
    private Map<K, V> map;

    public static <K, V> MapBuilder<K, V> newInstance() {
        return new MapBuilder<K, V>();
    }
    
    public MapBuilder<K, V> add(K key, V value) {
        map.put(key, value);
        return this;
    }
    
    public Map<K, V> build() {
        return Collections.unmodifiableMap(map);
    }
    
    private MapBuilder() {
        map = new HashMap<>();
    }

}
