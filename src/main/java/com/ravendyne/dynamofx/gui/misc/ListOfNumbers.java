package com.ravendyne.dynamofx.gui.misc;

import java.util.ArrayList;
import java.util.List;

public abstract class ListOfNumbers {
    protected final List<Number> values;
    protected double[] doubleArray;

    protected ListOfNumbers() {
        values = new ArrayList<>();
    }

    protected static Number parseNumber(String value) {
        Number number = null;
        
        if(value == null) {
            return null;
        }

        try {
            number = Double.valueOf((String) value);
        } catch (NumberFormatException ex) {
            // TODO log error
            ex.printStackTrace();
        }

        return number;
    }
    
    public List<Number> getValues() {
        return new ArrayList<>( values );
    }

    public double[] asDoubleArray() {

        if(doubleArray == null) {
            doubleArray = new double[values.size()];
            int idx = 0;
            for(Number value : values) {
                doubleArray[idx] = value.doubleValue();
                idx++;
            }
        }

        return doubleArray;
    }

}
