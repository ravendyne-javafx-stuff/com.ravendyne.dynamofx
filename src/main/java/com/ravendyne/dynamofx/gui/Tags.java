package com.ravendyne.dynamofx.gui;

public final class Tags {
    private Tags() {}

    // CONTROLS
    public static final String TAB = "Tab";

    // COMPONENTS
    public static final String PANE = "Pane";
    public static final String ANCHOR_PANE = "AnchorPane";
    public static final String BORDER_PANE = "BorderPane";
    public static final String FLOW_PANE = "FlowPane";
    public static final String GRID_PANE = "GridPane";
    public static final String HBOX = "HBox";
    public static final String STACK_PANE = "StackPane";
    public static final String TILE_PANE = "TilePane";
    public static final String VBOX = "VBox";
    public static final String ACCORDION = "Accordion";
    public static final String CHOICE_BOX = "ChoiceBox";
    public static final String LIST_VIEW = "ListView";
    public static final String SCROLL_PANE = "ScrollPane";
    public static final String SLIDER = "Slider";
    public static final String SPLIT_PANE = "SplitPane";
    public static final String TAB_PANE = "TabPane";
    public static final String TEXT_AREA = "TextArea";
    public static final String TEXT_FIELD = "TextField";
    public static final String PASSWORD_FIELD = "PasswordField";
    public static final String TREE_VIEW = "TreeView";
    public static final String LABEL = "Label";
    public static final String TITLED_PANE = "TitledPane";
    public static final String BUTTON = "Button";
    public static final String HYPERLINK = "Hyperlink";
    public static final String PROGRESS_BAR = "ProgressBar";

}
