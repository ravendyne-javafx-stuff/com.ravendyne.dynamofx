package com.ravendyne.dynamofx.gui.builders.component.labeled;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.builders.ComponentLabeledBuilder;

import javafx.scene.control.Button;

public final class FxButtonBuilder extends ComponentLabeledBuilder<Button> {

    private FxButtonBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxButtonBuilder newInstance(Map<String, CfgValue> parameters) {
        FxButtonBuilder builder = new FxButtonBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        Button button = new Button();

        rootComponent = idComponent = button;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
