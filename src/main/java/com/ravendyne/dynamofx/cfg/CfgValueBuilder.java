package com.ravendyne.dynamofx.cfg;

public interface CfgValueBuilder extends CfgValue {
    
//    void setNumber(Number value);

    void setString(String value);

    void setMapItem(String key, CfgValue value);

    void addListItem(CfgValue value);

    void addAllListItems(CfgValue value);

}
