package com.ravendyne.dynamofx;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.Cfg;
import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.builders.BuilderBase;

import javafx.scene.layout.Region;

public class FxBuilder {

    public static final String DEFAULT_ROOT_KEY = "gui";

    private FxBuilder() {
    }

    public static Region build(List<String> configSource) {
        return build(DEFAULT_ROOT_KEY, configSource);
    }

    public static Region build(String rootKey, List<String> configSource) {

        String lineSeparator = System.getProperty("line.separator", "\n");

        return build( rootKey, String.join(lineSeparator, configSource) );
    }

    public static Region build(String configSource) {
        return build(DEFAULT_ROOT_KEY, configSource);
    }

    public static Region build(String rootKey, String configSource) {
    	Region root = null;

        Cfg cfg = new Cfg(configSource);

        if(!cfg.getErrorMessage().isEmpty()) {
            throw new IllegalArgumentException("GUI configuration file has errors: " + cfg.getErrorMessage());
        }
        
        Map<String, CfgValue> configMap = cfg.get();
        root = createRoot(rootKey, configMap);

        return root;
    }

    public static Region createRoot(String rootKey, Map<String, CfgValue> parameters) {
        
        CfgValue guiConfig = parameters.get(rootKey);
        if(guiConfig != null && guiConfig.isMap()) {
            return BuilderBase.createComponent(guiConfig.getMap());
         }
        return null;
    }
}
