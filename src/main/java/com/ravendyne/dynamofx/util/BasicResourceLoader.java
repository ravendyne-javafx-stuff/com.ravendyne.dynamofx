package com.ravendyne.dynamofx.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.ravendyne.dynamofx.cfg.Cfg;
import com.ravendyne.dynamofx.cfg.CfgValue;

import javafx.scene.image.Image;
import javafx.scene.text.Font;

public class BasicResourceLoader implements IResourceLoader{
    
    // TODO handle different protocols for 'location' parameters in load methods

    public BasicResourceLoader() {
        
    }
    
    public String getPackagePath(Class<?> clazz) {
        String path = clazz.getPackage().getName().replace( ".", "/" );
        return "/" + path + "/";
    }
    
    public String getPackagePath() {
        return getPackagePath(getClass());
    }
    
    public static class ImageByteBuffer {
        public final ByteBuffer data;
        public final int width;
        public final int height;
        
        public ImageByteBuffer(ByteBuffer data, int width, int height) {
            this.data = data;
            this.width = width;
            this.height = height;
        }
    }

    public List<String> loadGlslSource( String location ) {

        List<String> lines = loadTextFile( location );

        for(int i=0; i< lines.size(); i++) {
            
            lines.set( i, lines.get( i ) + "\r\n");

        }

        return lines;

    }

    @Override
    public List<String> loadTextFile( String location ) {

        InputStream stream = getInputStream(location);
        
        List<String> result = loadTextFromStream(stream);
        
        try {
            stream.close();
        } catch (IOException e) {
        }

        return result;
    }

    @Override
    public List<String> loadTextResource(String location) {

        InputStream stream = getClass().getResourceAsStream(location);

        List<String> result = loadTextFromStream(stream);

        return result;
    }

    @Override
    public List<String> loadTextResource(Class<?> clazz, String location) {

        InputStream stream = clazz.getResourceAsStream(location);

        List<String> result = loadTextFromStream(stream);

        return result;
    }

    private List<String> loadTextFromStream(InputStream stream) {

        List<String> result = new ArrayList<>();
        
        if( stream != null ) {

            BufferedReader br = new BufferedReader( new InputStreamReader( stream ));
    
            br.lines().forEach((l) -> result.add(l));
            
            try {
                br.close();
            } catch (IOException e) {
                // we don't care what happens once we've loaded the content.
            }

        }
        return result;
    }

    @Override
    public BufferedImage loadImage( String location ) {

        InputStream stream = getInputStream(location);

        BufferedImage image = null;

        try {
            image = ImageIO.read( stream );
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return image;
    }

    @Override
    public ImageIcon loadImageIcon(String location) {

        final Class<? extends BasicResourceLoader> clazz = getClass();
        URL appIconLocation = clazz.getResource( location );

        if(appIconLocation != null) {
            return new ImageIcon( appIconLocation );
        }

        return null;

    }

    @Override
    public Image loadFxImage(String location) {
        if(location == null) {
            return null;
        }

        InputStream appIconStream = getInputStream( location );

        if(appIconStream != null) {
            return new Image( appIconStream );
        }

        return null;

    }

    protected InputStream getInputStream(String location) {

        InputStream stream = getClass().getResourceAsStream( location );

        if(stream == null) {

            try
            {
                Path path = Paths.get( location ).toAbsolutePath();
                stream = new FileInputStream( path.toFile() );
            }
            catch (FileNotFoundException e)
            {
                throw new IllegalArgumentException("'" + location + "' is not a valid resoruce or filesystem location.");
            }
        }

        return stream;
    }

    @Override
    public Font loadFont(String location, int size) {

        String fontUrl = getClass().getResource(location).toExternalForm();
        Font font = Font.loadFont(fontUrl, 12);

        return font;
    }

    @Override
    public Map<String, CfgValue> loadConfigFile(String location) {

        List< String > config = loadTextFile( location );

        return loadConfigFile( config );
    }

    @Override
    public Map<String, CfgValue> loadConfigFile(List< String > config) {

        String lineSeparator = System.getProperty("line.separator", "\n");
        Cfg cfg = new Cfg( String.join( lineSeparator, config ) );

        return cfg.get();
    }

}
