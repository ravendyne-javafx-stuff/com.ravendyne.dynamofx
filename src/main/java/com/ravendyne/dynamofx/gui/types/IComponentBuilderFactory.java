package com.ravendyne.dynamofx.gui.types;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;

import javafx.scene.layout.Region;

public interface IComponentBuilderFactory {
    IComponentBuilder<? extends Region> newInstance(Map<String, CfgValue> parameters);
}
