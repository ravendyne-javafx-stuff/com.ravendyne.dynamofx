package com.ravendyne.dynamofx.gui;

import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import com.ravendyne.dynamofx.DynamoFxComponent;
import com.ravendyne.dynamofx.TestResources;
import com.ravendyne.dynamofx.gui.types.Inject;

import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

@ExtendWith(ApplicationExtension.class)
public class SampleTestFx {
    
    @Inject TreeView<String> treeView;

    @Start
    public void start(Stage stage) {
        List<String> guiConfig = TestResources.loader.loadTextFile(TestResources.loader.getPackagePath(SampleTestFx.class) + "tree_view.dynamofx");
        
        Region node = DynamoFxComponent
            .newInstance("test", guiConfig)
            .setResourceLoader( TestResources.loader )
            .injectInto( this )
            .build();

        Scene scene = new Scene(node);

        stage.setScene(scene);
        stage.setTitle("TreeView test");

        buildGui();
        stage.show();
        
    }

    @SuppressWarnings("unchecked")
    protected void buildGui() {
        Objects.requireNonNull(treeView, "TreeView hasn't been injected");

        TreeItem<String> root = new TreeItem<String>("Root");
        root.setExpanded(true);

        TreeItem<String> bono = new TreeItem<String>("bono");
        bono.getChildren().addAll(
                new TreeItem<String>("bono 1"),
                new TreeItem<String>("bono 2"),
                new TreeItem<String>("bono 3")
            );

        root.getChildren().addAll(
            new TreeItem<String>("Item 1"),
            bono,
            new TreeItem<String>("Item 2"),
            new TreeItem<String>("Item 3")
        );

        treeView.setRoot(root);
    }

    @Test
    void blah() {
        
    }
}
