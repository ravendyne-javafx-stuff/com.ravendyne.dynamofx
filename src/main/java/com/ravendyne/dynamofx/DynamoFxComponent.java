package com.ravendyne.dynamofx;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.ravendyne.dynamofx.gui.ComponentBuilderRegistry;
import com.ravendyne.dynamofx.gui.ComponentInstanceRegistry;
import com.ravendyne.dynamofx.gui.ComponentInstanceRegistry.InstanceRegistry;
import com.ravendyne.dynamofx.util.IResourceLoader;

import javafx.scene.layout.Region;

public class DynamoFxComponent {
    private static Map<String, DynamoFxComponent> instanceRegistry = new HashMap<>();
    private final InstanceRegistry registry;
    private final String componentName;
    private final List<String> componentConfig;

    private Region rootNode;
    private Object injectionTarget;
    private String configRootKey;

    private DynamoFxComponent(String name, List<String> config) {
        componentName = name;
        registry = ComponentInstanceRegistry.INSTANCE.getRegistry( componentName );
        componentConfig = config;
        rootNode = null;
        injectionTarget = null;
        configRootKey = FxBuilder.DEFAULT_ROOT_KEY;
    }
    
    //
    // Factories
    //
    public static DynamoFxComponent newInstance(String name, List<String> config) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(config);

        DynamoFxComponent dynamoFxComponent = new DynamoFxComponent(name, config);
        instanceRegistry.put( name, dynamoFxComponent );
        return dynamoFxComponent;
    }

    //
    // Instance methods
    //
    public static DynamoFxComponent getInstance(String name) {
        return instanceRegistry.get( name );
    }
    
    public DynamoFxComponent injectInto(Object client) {
        injectionTarget = client;
        return this;
    }
    
    public DynamoFxComponent setConfigRoot(String rootKey) {
        configRootKey = rootKey;
        return this;
    }
    
    public DynamoFxComponent setResourceLoader(IResourceLoader loader) {
        ComponentBuilderRegistry.INSTANCE.setResourceLoader(loader);
        return this;
    }

    public Region build() {
        // set active registry so that build command on next line registers correctly
        // all created instances that have 'id' parameter set
        ComponentInstanceRegistry.INSTANCE.setActiveRegistry( componentName );
        rootNode = FxBuilder.build(configRootKey, componentConfig);

        if(injectionTarget != null) {
            registry.inject(injectionTarget);
        }

        return rootNode;
    }
}
