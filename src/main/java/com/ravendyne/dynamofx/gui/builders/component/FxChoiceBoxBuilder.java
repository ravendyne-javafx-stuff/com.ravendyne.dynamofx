package com.ravendyne.dynamofx.gui.builders.component;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.Tags;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.util.ClassUtil;

import javafx.scene.control.ChoiceBox;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.FloatStringConverter;

// FIXME update javadoc
/**
 * Builds {@link ChoiceBox} with value type set to {@link String} and converter set to {@link DefaultStringConverter}, by default.
 * <br><br>
 * These can be changed by setting parameters <code>type</code> and <code>converter</code> in config file, i.e.:
 * <pre>
 *   VBox {
 *     c <
 *       ChoiceBox {
 *         id: myBox
 *         type: java.lang.Float
 *         converter: javafx.util.converter.FloatStringConverter
 *         items [
 *           : 1.4
 *           : 3.128
 *           : 123.44
 *           : 10.0e4
 *         ]
 *       }
 *     >    
 *   }
 * </pre>
 * 
 * however, both <code>type</code> and <code>converter</code> have to be matched,
 * i.e. if <code>type</code> is {@link Float} then <code>converter</code> should be {@link FloatStringConverter}, or a class that 
 * extends <code>{@literal StringConverter<Float>}</code> etc.
 * <br>
 * If <code>type</code> and <code>converter</code> are not matched, {@link FxChoiceBoxBuilder} will throw {@link IllegalArgumentException} at component build time, i.e.:
 * 
 * <pre>
 *  java.lang.IllegalArgumentException: 'type' and 'converter' config parameters for ChoiceBox don't match: ChoiceBox 'type' is (java.lang.String) and 'converter' is for type (java.lang.Float).
 * </pre>
 * 
 * Also, if you plan to {@link Inject} the {@link ChoiceBox} to a class, declared class field type should match the <code>type</code> parameter from config file.
 * For example, for above config sample, a field should be declared like this:
 * 
 * <pre>
 * {@literal @Inject ChoiceBox<Float> myBox; }
 * </pre>
 * <br>
 * You could declare it with different generic type, i.e. {@code ChoiceBox<String>}, and the field would still be injected, but the result would be unpredictable
 * when you try to set/get value from <code>myBox</code> or add items, etc.
 * <br>
 * <br>
 * This matching has to be done manually since generic Java types are not <a href="https://docs.oracle.com/javase/tutorial/java/generics/erasure.html">reified</a>.
 *
 */
public final class FxChoiceBoxBuilder extends ComponentBuilder<ChoiceBox<?>> {

    private FxChoiceBoxBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxChoiceBoxBuilder newInstance(Map<String, CfgValue> parameters) {
        FxChoiceBoxBuilder builder = new FxChoiceBoxBuilder(parameters);
        return builder;
    }

    private <T> void buildChoiceBox( Class< T > choiceBoxTypeClass ) {
        ChoiceBox< T > choiceBox = new ChoiceBox<T>();

        StringConverter<T> ac = createConverter(choiceBoxTypeClass, choiceBox);
        
        List< CfgValue > itemsList = getValueList( Attributes.ITEMS, parameters );

        for( CfgValue itemAsString : itemsList ) {
            if( itemAsString.isString() ) {
                T value = ac.fromString( itemAsString.getString() );
                choiceBox.getItems().add( value );
            }
        }

        rootComponent = idComponent = choiceBox;
    }

    private <T> StringConverter<T> createConverter(Class<T> choiceBoxTypeClass, ChoiceBox<T> choiceBox) {
        String converterType = getStringValue( Attributes.CONVERTER, DefaultStringConverter.class.getName(), parameters );
        Class< ? > converterTypeClass = ClassUtil.loadClass( converterType, Attributes.CONVERTER );
        Object converter = ClassUtil.newInstance(converterTypeClass, Attributes.CONVERTER);

        if( !( converter instanceof StringConverter ) ) {
            throw new IllegalArgumentException("Class '" + converterType + "' has to extend '" + StringConverter.class.getName() + "' in order to be acceptible as '"+Attributes.CONVERTER+"'.");
        }

        List< Class< ? > > args = ClassUtil.getTypeArguments( StringConverter.class, converter.getClass() );
        // since we have already made sure that converter is instanceof StringConverter, args should have one element, the converter's type
        // if it doesn't, then something it wrong with ClassUtil.getTypeArguments()...
        Class< ? > converterHandleType = args.get( 0 );
        if( choiceBoxTypeClass != converterHandleType ) {
            throw new IllegalArgumentException(
                    "'"+Attributes.TYPE+"' and '"+Attributes.CONVERTER+"' config parameters for "+Tags.CHOICE_BOX+" don't match: "+Tags.CHOICE_BOX+
                    " '"+Attributes.TYPE+"' is (" + choiceBoxTypeClass.getName() + ") and '"+
                    Attributes.CONVERTER+"' is for type (" + converterHandleType.getName() + ").");
        }

        @SuppressWarnings( "unchecked" )
        // it should be safe to do this cast since we checked if converter instanceof StringConverter above
        StringConverter<T> ac = (StringConverter<T>)( converter );
        choiceBox.setConverter( ac );
        return ac;
    }

    @Override
    public void buildComponent() {

        String choiceBoxType = getStringValue( Attributes.TYPE, "java.lang.String", parameters );

        Class< ? > choiceBoxTypeClass = ClassUtil.loadClass( choiceBoxType, Attributes.TYPE );
        
        buildChoiceBox( choiceBoxTypeClass );
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
