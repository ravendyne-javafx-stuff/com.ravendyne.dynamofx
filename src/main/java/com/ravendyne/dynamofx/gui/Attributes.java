package com.ravendyne.dynamofx.gui;

public final class Attributes {
    private Attributes() {}

    public static final String ID = "id";
    public static final String CONTROL_LIST = "c";
    
    // Labeled
    public static final String TEXT = "text";
    public static final String GRAPHICS = "graphics";
    
    // TextInput
    public static final String EDITABLE = "editable";
    public static final String PROMPT_TEXT = "promptText";
    public static final String FONT = "font";
    
    // Region
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String SIZE = "size";
    public static final String CSS_CLASS = "cssClass";
    public static final String PADDING = "padding";
    
    // BorderPane
    public static final String BOTTOM = "bottom";
    public static final String TOP = "top";
    public static final String LEFT = "left";
    public static final String RIGHT = "right";
    public static final String CENTER = "center";
    
    // FlowPane
    public static final String HGAP = "hgap";
    public static final String VGAP = "vgap";
    public static final String WRAP = "wrap";
    
    // GridPane
    public static final String ALIGN = "align";
    public static final String COLUMN_INDEX = "column";
    public static final String ROW_INDEX = "row";
    public static final String H_ALIGNMENT = "halign";
    public static final String V_ALIGNMENT = "valign";
    
    // TilePane
    public static final String ORIENTATION = "orientation";
    
    // Tab
    public static final String CONTENT = "content";
    public static final String TOOLTIP = "tooltip";
    
    // TextArea
    public static final String COLS = "cols";
    public static final String ROWS = "rows";
    
    // Accordion
    public static final String PANES = "panes";
    
    // ChoiceBox
    public static final String ITEMS = "items";
    public static final String CONVERTER = "converter";
    public static final String TYPE = "type";
    
    // SplitPane
    public static final String DIVIDERS = "dividers";
    
    // Slider
    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String VALUE = "value";
    
    // TabPane
    public static final String TABS = "tabs";
    
    // HBox, VBox
    public static final String SPACING = "spacing";
    public static final String FILL_WIDTH = "fillWidth";
    public static final String FILL_HEIGHT = "fillHeight";
	public static final String VGROW = "vgrow";
	public static final String HGROW = "hgrow";

	public static final String MARGIN = "margin";

    // TitledPane
    public static final String ANIMATED = "animated";
	public static final String COLLAPSIBLE = "collapsible";
	public static final String EXAPNDED = "expanded";
	
	// SplitPane
    public static final String RESIZABLE_WITH_PARENT = "resizableWithParent";
}
