package com.ravendyne.dynamofx.gui.builders;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.types.IComponentBuilder;

import javafx.scene.layout.Region;

// javafx.scene.layout.Region
public abstract class BuilderBase<T extends Region> extends Builder implements IComponentBuilder<T> {

    protected T rootComponent;
    protected T idComponent;

    protected BuilderBase(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    @Override
    public T getRootComponent() {
        return rootComponent;
    }

    @Override
    public T getIdComponent() {
        return idComponent;
    }
    
    @Override
    public void build() {
        buildComponent();
        setComponentParameters();
    }
    
    protected void setParameters(T region) {};
    
    protected abstract void buildComponent();
    protected abstract void setComponentParameters();

}
