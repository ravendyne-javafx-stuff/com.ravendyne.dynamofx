package com.ravendyne.dynamofx.gui.types;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;

public interface IControlBuilderFactory {
    IControlBuilder<?> newInstance(Map<String, CfgValue> parameters);
}
