package com.ravendyne.dynamofx.gui.builders.component;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

import com.ravendyne.dynamofx.TestResources;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.DynamoFxComponent;

import javafx.scene.control.Accordion;

@ExtendWith(ApplicationExtension.class)
public class AccordionTest {
    static List<String> guiConfig;

    @Inject Accordion accordion;
    
    @BeforeAll
    public static void init() {
        guiConfig = TestResources.loader.loadConfig(AccordionTest.class, "accordion.dynamofx");
    }

    @Test
    public void test1() {
        DynamoFxComponent
            .newInstance("test", guiConfig)
            .setResourceLoader( TestResources.loader )
            .injectInto( this )
            .build();
        
        Assertions.assertNotNull(accordion);
        Assertions.assertEquals(2, accordion.getPanes().size());
    }

    @Test
    public void test2() {
        DynamoFxComponent builder = DynamoFxComponent
        .newInstance("test", guiConfig)
        .setConfigRoot("fails")
        .setResourceLoader( TestResources.loader )
        .injectInto( this );

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            builder.build();
        });
    }
}
