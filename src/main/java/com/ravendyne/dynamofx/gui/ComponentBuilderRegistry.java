package com.ravendyne.dynamofx.gui;

import java.util.HashMap;
import java.util.Map;

import com.ravendyne.dynamofx.DynamoFxResources;
import com.ravendyne.dynamofx.gui.types.IComponentBuilderFactory;
import com.ravendyne.dynamofx.util.IResourceLoader;

public enum ComponentBuilderRegistry {
    INSTANCE;

    private final Map<String, IComponentBuilderFactory> registrar;
    private IResourceLoader resourceLoader;

    ComponentBuilderRegistry() {
        registrar = new HashMap<>();
        registerDefaults();
        resourceLoader = DynamoFxResources.loader;
    }
    
    private void registerDefaults() {
        for( String tag : Builders.components.keySet() ) {
            register(tag, Builders.components.get(tag));
        }
    }

    public IComponentBuilderFactory getFactoryFor(String tag) {
        return registrar.get(tag);
    }
    
    public void register(String tag, IComponentBuilderFactory builder) {
        registrar.put(tag, builder);
    }
    
    public void setResourceLoader(IResourceLoader loader) {
        resourceLoader = loader;
    }

    public IResourceLoader getResourceLoader() {
        return resourceLoader;
    }
}
