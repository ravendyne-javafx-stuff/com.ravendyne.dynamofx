package com.ravendyne.dynamofx.gui.builders.component;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.Tags;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public final class FxTabPaneBuilder extends ComponentBuilder<TabPane> {

    private FxTabPaneBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxTabPaneBuilder newInstance(Map<String, CfgValue> parameters) {
        FxTabPaneBuilder builder = new FxTabPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        TabPane component = new TabPane();

        List<Map<String, CfgValue>> panesListParameters = getList(Attributes.TABS, parameters);

        for(Map<String, CfgValue> itemParameters : panesListParameters) {
            if(!itemParameters.isEmpty()) {
                String tag = itemParameters.keySet().iterator().next();
                if(!tag.equals( Tags.TAB )) {
                    throw new IllegalArgumentException( Tags.TAB_PANE+" '"+Attributes.TABS+"' attribute can only contain elements of type '"+Tags.TAB+"'. Invalid element tag was '" + tag + "'" );
                }

                Object tab = createControl(itemParameters);
                component.getTabs().add((Tab) tab);
            }
        }

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
