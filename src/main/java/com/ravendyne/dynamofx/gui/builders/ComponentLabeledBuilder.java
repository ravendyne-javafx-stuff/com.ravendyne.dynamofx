package com.ravendyne.dynamofx.gui.builders;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.ComponentBuilderRegistry;

import javafx.scene.control.Labeled;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

//javafx.scene.layout.Region -> javafx.scene.control.Control -> javafx.scene.control.Labeled
public abstract class ComponentLabeledBuilder<T extends Labeled> extends ComponentBuilder<T> {

    protected ComponentLabeledBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    @Override
    protected void setParameters(T controlLabeled) {
        super.setParameters( controlLabeled );

        String text = getStringValue(Attributes.TEXT, parameters);
        controlLabeled.setText( text );
        
        String graphicsUrl = getStringValue(Attributes.GRAPHICS, parameters);
        if(graphicsUrl != null) {
            Image image = ComponentBuilderRegistry.INSTANCE.getResourceLoader().loadFxImage(graphicsUrl);
            controlLabeled.setGraphic(new ImageView(image));
        }
    }

}
