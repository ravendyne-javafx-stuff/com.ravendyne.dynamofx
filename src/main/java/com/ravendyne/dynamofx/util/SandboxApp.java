package com.ravendyne.dynamofx.util;

import java.util.ArrayList;
import java.util.List;

import com.ravendyne.dynamofx.DynamoFxComponent;
import com.ravendyne.dynamofx.DynamoFxResources;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public abstract class SandboxApp extends Application {

    protected String configName;
    protected String title;
    protected List<String> styleSheets;
    
    public SandboxApp() {
        styleSheets = new ArrayList<>();
    }
    
    protected abstract void run();

    @Override
    public void start(Stage primaryStage) throws Exception {
        List<String> guiConfig = DynamoFxResources.loader.loadTextFile(configName);
        
        Region node = DynamoFxComponent
            .newInstance("sandbox", guiConfig)
            .setResourceLoader( DynamoFxResources.loader )
            .injectInto( this )
            .build();
        
        Scene scene = new Scene(node);
        scene.getStylesheets().addAll(styleSheets);

        primaryStage.setScene(scene);
        primaryStage.setTitle(title);
        Image icon = DynamoFxResources.loader.loadFxImage("/icons/dfxicon.png");
        primaryStage.getIcons().add(icon);
        primaryStage.setOnShown( new EventHandler<WindowEvent>() {
            @Override
            public void handle( WindowEvent event ) {
                run();
            }
        } );

        primaryStage.show();
    }

}
