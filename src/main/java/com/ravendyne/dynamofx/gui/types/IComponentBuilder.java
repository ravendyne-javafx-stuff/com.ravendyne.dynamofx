package com.ravendyne.dynamofx.gui.types;

import javafx.scene.layout.Region;

public interface IComponentBuilder<T extends Region> {
    T getRootComponent();
    T getIdComponent();
    void build();
}
