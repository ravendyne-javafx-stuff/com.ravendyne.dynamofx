package com.ravendyne.dynamofx.gui.builders.component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.util.ClassUtil;

import javafx.scene.control.ListView;

//FIXME update javadoc
/**
 * Builds {@link ListView} with value type set to {@link String} by default.
 * <br><br>
 * This can be changed by setting parameter <code>type</code> in config file, i.e.:
 * <pre>
 *   VBox {
 *     c <
 *       ListView {
 *         id: myListView
 *         type: java.lang.Float
 *         items [
 *           : 1.4
 *           : 3.128
 *           : 123.44
 *           : 10.0e4
 *         ]
 *       }
 *     >
 *   }
 * </pre>
 * 
 * You can use any class for <code>type</code> parameter as long as it has {@code public static T valueOf(String)} method, where {@code T} is
 * the class used for the <code>type</code> parameter. For example, see {@link Float#valueOf(String)}.
 * 
 * Also, if you plan to {@link Inject} the {@link ListView} into a class, declared class field type should match the <code>type</code> parameter from config file.
 * For example, for above config sample, a field should be declared like this:
 * 
 * <pre>
 * {@literal @Inject ListView<Float> myListView; }
 * </pre>
 * <br>
 * You could declare it with different generic type, i.e. {@code ListView<String>}, and the field would still be injected, but the result would be unpredictable
 * when you try to set/get value from <code>myListView</code> or add items, etc.
 * <br>
 * <br>
 * This matching has to be done manually since generic Java types are not <a href="https://docs.oracle.com/javase/tutorial/java/generics/erasure.html">reified</a>.
 *
 */
public final class FxListViewBuilder extends ComponentBuilder<ListView<?>> {

    private FxListViewBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxListViewBuilder newInstance(Map<String, CfgValue> parameters) {
        FxListViewBuilder builder = new FxListViewBuilder(parameters);
        return builder;
    }
    
    @SuppressWarnings("unchecked")
    // we can suppress unchecked warning since getStaticValueOfMethod() will only find "valueOf" method that returns type T
    private <T> T callValueOf( Method valueOfMethod, String string ) {
        T result = null;

        try {
            result = (T) valueOfMethod.invoke(null, string);
        } catch (IllegalAccessException | IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // valueOf() conversion error
        }
        
        return result;
    }
    
    private Method getValueOfMethod( Class< ? > typeClass ) {
        Method valueOfMethod = ClassUtil.getStaticValueOfMethod(typeClass);
        if(valueOfMethod == null) {
            throw new IllegalArgumentException(typeClass.getName() + " has to have either valueOf(String) or valueOf(Object) method.");
        }
        return valueOfMethod;
    }

    private <T> void buildListView( Class< T > listViewTypeClass ) {
        ListView< T > listView = new ListView<T>();
        Method valueOfMethod = getValueOfMethod(listViewTypeClass);
        
        List< CfgValue > itemsList = getValueList( Attributes.ITEMS, parameters );

        for( CfgValue itemAsString : itemsList ) {
            if( itemAsString.isString() ) {
                listView.getItems().add( callValueOf(valueOfMethod, itemAsString.getString()) );
            }
        }

        rootComponent = idComponent = listView;
    }

    @Override
    public void buildComponent() {

        String listViewType = getStringValue( Attributes.TYPE, "java.lang.String", parameters );

        Class< ? > listViewTypeClass = ClassUtil.loadClass( listViewType, Attributes.TYPE );
        
        buildListView( listViewTypeClass );
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
