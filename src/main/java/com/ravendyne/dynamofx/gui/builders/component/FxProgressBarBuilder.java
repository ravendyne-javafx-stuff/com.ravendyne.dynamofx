package com.ravendyne.dynamofx.gui.builders.component;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;

import javafx.scene.control.ProgressBar;

public final class FxProgressBarBuilder extends ComponentBuilder<ProgressBar> {

    private FxProgressBarBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxProgressBarBuilder newInstance(Map<String, CfgValue> parameters) {
        FxProgressBarBuilder builder = new FxProgressBarBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {
        
        ProgressBar component = new ProgressBar();

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
