package com.ravendyne.dynamofx.gui.builders.control;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.ControlBuilderRegistry;
import com.ravendyne.dynamofx.gui.builders.ControlBuilderBase;

import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @see <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/Tab.html">JavaFX Tab</a>
 */
public final class FxTabBuilder extends ControlBuilderBase<Tab> {

    private FxTabBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxTabBuilder newInstance(Map<String, CfgValue> parameters) {
        FxTabBuilder builder = new FxTabBuilder(parameters);
        return builder;
    }

    @Override
    public void buildControl() {
        
        Tab control = new Tab();

        control.setContent(createComponent(getMap(Attributes.CONTENT, parameters)));

        controlInstance = control;
    }

    @Override
    protected void setControlParameters() {
        setParameters(controlInstance);

        applyStringValue(Attributes.TEXT, parameters, (text) -> controlInstance.setText( text ) );

        applyStringValue(Attributes.TOOLTIP, parameters, (tooltipText) -> {
            Tooltip tooltip = new Tooltip(tooltipText);
            controlInstance.setTooltip( tooltip );
        });

        applyStringValue(Attributes.GRAPHICS, parameters, (graphicsUrl) -> {
            Image image = ControlBuilderRegistry.INSTANCE.getResourceLoader().loadFxImage(graphicsUrl);
            controlInstance.setGraphic(new ImageView(image));
        });
    }

}
