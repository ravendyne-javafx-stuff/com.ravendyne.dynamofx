package com.ravendyne.dynamofx.gui.builders.component;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentBuilder;

import javafx.scene.control.Slider;

public final class FxSliderBuilder extends ComponentBuilder<Slider> {

    private FxSliderBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxSliderBuilder newInstance(Map<String, CfgValue> parameters) {
        FxSliderBuilder builder = new FxSliderBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        Slider slider = new Slider();

        rootComponent = idComponent = slider;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        applyNumericValue(Attributes.MIN, parameters, (min) -> rootComponent.setMin( min.doubleValue() ) );

        applyNumericValue(Attributes.MAX, parameters, (max) -> rootComponent.setMax( max.doubleValue() ) );

        applyNumericValue(Attributes.VALUE, parameters, (value) -> rootComponent.setValue( value.doubleValue() ) );
    }

}
