package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.List;
import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public final class FxHBoxBuilder extends RegionBuilder<HBox> {

    public FxHBoxBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxHBoxBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxHBoxBuilder builder = new FxHBoxBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        HBox layout = new HBox();

        /*
         * https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/HBox.html
         * TODO hgrow, margin
         * 
         *      HBox.setHgrow(component, Priority.ALWAYS);
         *      component.setMaxWidth(Double.MAX_VALUE);
         * 
         */
//        layout.getChildren().addAll(createComponents(getList(Attributes.CONTROL_LIST, parameters)));
        List< Map< String, CfgValue > > componentConfigs = getList(Attributes.CONTROL_LIST, parameters);
        for(Map< String, CfgValue > config : componentConfigs){

            Region component = createComponent( config );
            
            // createComponent() would throw an exception if
            // getting attributes wouldn't have worked
            String tag = config.keySet().iterator().next();
            Map<String, CfgValue> attributes = getMap(tag, config);

            applyEnumValue(Priority.class, Attributes.HGROW, attributes, (hGrow) -> {
                HBox.setHgrow( component, hGrow );
                component.setMaxWidth(Double.MAX_VALUE);
            });

            layout.getChildren().add( component );
        }
        
        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        applyNumericValue(Attributes.SPACING, parameters, (spacing) -> rootComponent.setSpacing( spacing.doubleValue() ) );

        applyBooleanValue(Attributes.FILL_HEIGHT, parameters, (fillHeightValue) -> rootComponent.setFillHeight( fillHeightValue ) );

        applyEnumValue(Pos.class, Attributes.V_ALIGNMENT, parameters, (alignment) -> rootComponent.setAlignment( alignment ) );
    }

}
