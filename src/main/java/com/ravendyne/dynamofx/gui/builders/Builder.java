package com.ravendyne.dynamofx.gui.builders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.ComponentBuilderRegistry;
import com.ravendyne.dynamofx.gui.ComponentInstanceRegistry;
import com.ravendyne.dynamofx.gui.ControlBuilderRegistry;
import com.ravendyne.dynamofx.gui.misc.Pair;
import com.ravendyne.dynamofx.gui.misc.Quad;
import com.ravendyne.dynamofx.gui.types.IComponentBuilder;
import com.ravendyne.dynamofx.gui.types.IComponentBuilderFactory;
import com.ravendyne.dynamofx.gui.types.IControlBuilder;
import com.ravendyne.dynamofx.gui.types.IControlBuilderFactory;

import javafx.geometry.Insets;
import javafx.scene.layout.Region;

public abstract class Builder {
    protected Map<String, CfgValue> parameters;

    protected Builder(Map<String, CfgValue> parameters) {
        this.parameters = parameters;
    }
    
    /////////////////////////////////////////////////////////////////////////////////
    //
    // Component/control creators
    //
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a component using a component builder given by key value from {@code parameters} map.
     * {@code parameters} map must have only one key. Key should be equal to the name of the component that is to be created.
     * {@code parameters.get(key)} should return a {@link CfgValue} that is a map. The map contains all the attributes for the component.
     * <br><br>
     * 
     * @param parameters a map with just one key that is associated with a value which is a map
     * @return root component of the created component
     */
    public static Region createComponent(Map<String, CfgValue> parameters) {
        IComponentBuilderFactory factory = null;

        if(!parameters.keySet().iterator().hasNext()) {
            return null;
        }
        String tag = parameters.keySet().iterator().next();
        Map<String, CfgValue> attributes = parameters.get(tag).getMap();
        if(attributes == null) {
            throw new IllegalArgumentException("Tag '" + tag + "' in GUI configuration doesn't have a map as its value");
        }

        factory = ComponentBuilderRegistry.INSTANCE.getFactoryFor(tag);
        if(factory == null) {
            throw new IllegalArgumentException("Unknown tag '" + tag + "'");
        }

        IComponentBuilder<?> builder = factory.newInstance(attributes);
        builder.build();
        Region component = builder.getRootComponent();

        String id = getStringValue(Attributes.ID, attributes);
        if(id != null) {
        	Region idComponent = builder.getIdComponent();
            idComponent.setId(id);
            ComponentInstanceRegistry.INSTANCE.register(id, idComponent);
        }

        return component;
    }

    protected static List<Region> createComponents(List<Map<String, CfgValue>> children) {
        List<Region> componentList = new ArrayList<>();

        for(Map<String, CfgValue> child: children) {
        	Region component = createComponent(child);
            if(component != null) {
                componentList.add(component);
            }
        }
        
        return componentList;
    }

    /**
     * Creates an instance of class from javafx.scene.control package which has java.lang.Object as direct superclass,
     * for example {@link javafx.scene.control.Tab} or {@link javafx.scene.control.MenuItem} etc.
     * The control is created using a control builder given by {@code t} parameter from {@code parameters} map
     * <br>.
     * Returns {@code null} if {@code t} parameter can't be found in the {@code parameters} map.
     * 
     * @param parameters
     * @return
     */
    public static Object createControl(Map<String, CfgValue> parameters) {
        IControlBuilderFactory factory = null;

        if(!parameters.keySet().iterator().hasNext()) {
            return null;
        }
        String tag = parameters.keySet().iterator().next();
        Map<String, CfgValue> attributes = parameters.get(tag).getMap();
        if(attributes == null) {
            throw new IllegalArgumentException("Tag '" + tag + "' in GUI configuration doesn't have a map as its value");
        }

        factory = ControlBuilderRegistry.INSTANCE.getFactoryFor(tag);
        if(factory == null) {
            throw new IllegalArgumentException("Unknown tag '" + tag + "'");
        }

        IControlBuilder<?> builder = factory.newInstance(attributes);
        builder.build();
        Object component = builder.getControl();

        return component;
    }

    protected static List<Object> createControls(List<Map<String, CfgValue>> children) {
        List<Object> controlList = new ArrayList<>();

        for(Map<String, CfgValue> child: children) {
            Object control = createControl(child);
            if(control != null) {
                controlList.add(control);
            }
        }
        
        return controlList;
    }

    /////////////////////////////////////////////////////////////////////////////////
    //
    // Parameter getters
    //
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets a {@link String} type value from the {@code parameters}. If parameters contain a value
     * which matches given {@code key} but the value is not of a {@link String} type, the method will
     * return {@code null}. If parameters don't have an entry with the given key, null is returned.
     * 
     * @param key parameter key
     * @param parameters map of parameters to search through
     * @return {@link String} value for the given key or, <br>null - if parameters do not contain an 
     * entry that matches given key, or they do but the value is not of {@link String} type
     */
    protected static String getStringValue(String key, Map<String, CfgValue> parameters) {
        return getStringValue(key, null, parameters);
    }

    protected static String getStringValue(String key, String defaultValue, Map<String, CfgValue> parameters) {
        CfgValue value = parameters.get(key);

        if(value != null && value.isString()) {
            return sanitizeString( value.getString() );
        }
        
        return defaultValue;
    }

    /**
     * Removes leading and trailing spaces from {@code value} parameter,
     * and then strips leading/trailing double/single quotes, if any.
     * @param value
     * @return
     */
    protected static String sanitizeString( String value ) {
        String result = value.trim();
        int beginIndex = 0;
        int endIndex = result.length();
        if(result.startsWith( "\"" ) || result.startsWith( "'" )) {
            beginIndex++;
        }
        if(result.endsWith( "\"" ) || result.endsWith( "'" )) {
            endIndex--;
        }
        return result.substring( beginIndex, endIndex );
    }

    protected static Number getNumericValue(String key, Map<String, CfgValue> parameters) {
        return getNumericValue(key, null, parameters);
    }

    protected static Number getNumericValue(String key, Number defaultValue, Map<String, CfgValue> parameters) {
        Number number = defaultValue;
        
        String stringValue = getStringValue( key, parameters );
        if(stringValue != null) {
            try {
                number = Double.valueOf( stringValue );
            } catch (NumberFormatException dex) {
                try {
                    number = Long.valueOf( stringValue );
                } catch (NumberFormatException lex) {
                    // Method's contract is to return defaultValue parameter if number can't be parsed
                    // so we just ignore number format exception here
                }
            }
        }
        
        return number;
    }

    protected static List<Map<String, CfgValue>> getList(String key, Map<String, CfgValue> parameters) {
        CfgValue value = parameters.get(key);

        if( value != null && value.isList() ) {
            return value.getList();
        }

        return new ArrayList<>();
    }

    protected static List<CfgValue> getValueList(String key, Map<String, CfgValue> parameters) {
        CfgValue value = parameters.get(key);

        if( value != null && value.isValueList() ) {
            return value.getValueList();
        }

        return new ArrayList<>();
    }

    protected static Map<String, CfgValue> getMap(String key, Map<String, CfgValue> parameters) {
        CfgValue value = parameters.get(key);

        if( value != null && value.isMap() ) {
            return value.getMap();
        }

        return new HashMap<>();
    }
    
    protected static <T extends Enum<T>> T getEnumValue(Class<T> enumType, String key, Map<String, CfgValue> parameters) {
        T result = null;

        String value = getStringValue(key, parameters);

        if(value != null) {
            value = value.trim().toUpperCase();

            try {

                result = Enum.valueOf(enumType, value);

            } catch (IllegalArgumentException ex) {
                // TODO use logger?
                System.err.println("Value '" + value +"' doesn't exist in '" + enumType.getName() + "'" );
            }
        }
        
        return result;
    }

    /////////////////////////////////////////////////////////////////////////////////
    //
    // Parameter applicators
    //
    /////////////////////////////////////////////////////////////////////////////////

    protected static void applyNumericValue(String key, Map<String, CfgValue> parameters, Consumer<Number> applicator) {
        Number value = getNumericValue( key, parameters );
        if(value != null) {
            applicator.accept( value );
        }
    }
    
    protected static <T extends Enum<T>> void applyEnumValue(Class<T> enumType, String key, Map<String, CfgValue> parameters, Consumer<T> applicator) {
        T value = getEnumValue(enumType, key, parameters);
        if(value != null) {
            applicator.accept( value );
        }
    }

    protected static void applyStringValue(String key, Map<String, CfgValue> parameters, Consumer<String> applicator) {
        String value = getStringValue( key, parameters );
        if(value != null) {
            applicator.accept( value );
        }
    }

    protected static void applyBooleanValue(String key, Map<String, CfgValue> parameters, Consumer<Boolean> applicator) {
        String value = getStringValue( key, parameters );
        if(value != null) {
            applicator.accept( Boolean.valueOf(value) );
        }
    }
    
    protected static void applyInsetsValue(String key, Map<String, CfgValue> parameters, Consumer<Insets> applicator) {
        String insetsString = getStringValue(key, parameters);

        if(insetsString != null) {
        	Insets insets;
            Quad insetsParam = Quad.valueOf(insetsString);

            if(insetsParam.getX() != null && insetsParam.getY() != null &&
                    insetsParam.getZ() != null && insetsParam.getW() != null) {
                // top, right, bottom, left
                insets = new Insets(
                        insetsParam.getX().doubleValue(), 
                        insetsParam.getY().doubleValue(), 
                        insetsParam.getZ().doubleValue(), 
                        insetsParam.getW().doubleValue());

                applicator.accept(insets);

            } else if(insetsParam.getX() != null) {
                // all four same value
                insets = new Insets(insetsParam.getX().doubleValue());

                applicator.accept(insets);

            }
        }
    }

    protected static void applyNumberPairValue(String key, Map<String, CfgValue> parameters, Consumer<Pair> applicator) {
        String pairString = getStringValue(key, parameters);
        if(pairString != null) {
            Pair pairParam = Pair.valueOf(pairString);
            if(pairParam.getX() != null && pairParam.getY() != null) {
            	applicator.accept(pairParam);
            }
        }
    }
}
