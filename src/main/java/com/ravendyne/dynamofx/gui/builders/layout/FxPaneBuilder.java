package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public final class FxPaneBuilder extends RegionBuilder<Pane> {

    public FxPaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static <T extends Region> FxPaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxPaneBuilder builder = new FxPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        Pane layout = new Pane();

        layout.getChildren().addAll(createComponents(getList(Attributes.CONTROL_LIST, parameters)));
        
        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
