package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.scene.layout.AnchorPane;

public final class FxAnchorPaneBuilder extends RegionBuilder<AnchorPane> {

    public FxAnchorPaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxAnchorPaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxAnchorPaneBuilder builder = new FxAnchorPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        AnchorPane layout = new AnchorPane();

        layout.getChildren().addAll(createComponents(getList(Attributes.CONTROL_LIST, parameters)));

        // TODO configure top/left/right/bottomAnchor on individual children
        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/AnchorPane.html
//        AnchorPane.setTopAnchor( child, 10.0 );
        
        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
