package com.ravendyne.dynamofx.gui.builders.component.textinput;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

import com.ravendyne.dynamofx.TestResources;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.DynamoFxComponent;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

@ExtendWith(ApplicationExtension.class)
public class TextInputTest {
    static List<String> guiConfig;
    
    @Inject TextField textField;
    @Inject PasswordField passwordField;
    @Inject TextArea textArea;
    
    @BeforeAll
    public static void init() {
        guiConfig = TestResources.loader.loadConfig(TextInputTest.class, "text_input.dynamofx");
    }
    
    @DisplayName("TextInput")
    @Test
    public void test1() {
        DynamoFxComponent
        .newInstance("test", guiConfig)
        .setResourceLoader( TestResources.loader )
        .setConfigRoot("textInput")
        .injectInto( this )
        .build();
        
        Assertions.assertNotNull(textField);
        Assertions.assertEquals("some text", textField.getText());
        Assertions.assertEquals("a prompt", textField.getPromptText());
        Assertions.assertFalse(textField.isEditable());
        Assertions.assertEquals("Open Sans Regular", textField.getFont().getName());
    }
    
    @DisplayName("TextField")
    @Test
    public void test2() {
        DynamoFxComponent
        .newInstance("test", guiConfig)
        .setResourceLoader( TestResources.loader )
        .setConfigRoot("textField")
        .injectInto( this )
        .build();
        
        Assertions.assertNotNull(textField);
        Assertions.assertEquals(22, textField.getPrefColumnCount());
    }
    
    @DisplayName("PasswordField")
    @Test
    public void test3() {
        DynamoFxComponent
        .newInstance("test", guiConfig)
        .setResourceLoader( TestResources.loader )
        .setConfigRoot("passwordField")
        .injectInto( this )
        .build();
        
        Assertions.assertNotNull(passwordField);
        Assertions.assertEquals(33, passwordField.getPrefColumnCount());
    }
    
    @DisplayName("TextArea")
    @Test
    public void test4() {
        DynamoFxComponent
        .newInstance("test", guiConfig)
        .setResourceLoader( TestResources.loader )
        .setConfigRoot("textArea")
        .injectInto( this )
        .build();
        
        Assertions.assertNotNull(textArea);
        Assertions.assertEquals(43, textArea.getPrefColumnCount());
        Assertions.assertEquals(10, textArea.getPrefRowCount());
        Assertions.assertFalse(textArea.isWrapText());
    }

}
