https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
https://docs.oracle.com/javase/8/javafx/api/

CSS REF:
https://docs.oracle.com/javase/8/javafx/api/javafx/scene/doc-files/cssref.html
https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/css_tutorial.htm
https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/apply-css.htm

GET STARTED:
https://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-architecture.htm

https://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-overview.htm
https://docs.oracle.com/javase/8/javafx/get-started-tutorial/get_start_apps.htm

GRAPHICS:
https://docs.oracle.com/javase/8/javafx/graphics-tutorial/javafx-3d-graphics.htm


LAYOUT:
https://docs.oracle.com/javase/8/javafx/layout-tutorial/index.html
https://docs.oracle.com/javase/8/javafx/layout-tutorial/builtin_layouts.htm

UI:
https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/index.html
https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/ui_controls.htm
https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/samples.htm

EFFECTS:
https://docs.oracle.com/javase/8/javafx/visual-effects-tutorial/animations.htm
https://docs.oracle.com/javase/8/javafx/visual-effects-tutorial/visual_effects.htm

SELF-CONTAINED DEPLOYMENT:
https://docs.oracle.com/javase/8/docs/technotes/guides/deploy/self-contained-packaging.html



MISC:

--------
UE4 Blueprints-like CSS/HTML
https://docs.unrealengine.com/latest/INT/BlueprintAPI/Audio/PlaySoundatLocation/index.html
https://docs.unrealengine.com/latest/INT/BlueprintAPI/Appearance/GetDynamicMaterial/index.html
https://docs.unrealengine.com/latest/INT/BlueprintAPI/index.html

https://docs.unrealengine.com/latest/INT/Engine/Blueprints/UserGuide/CheatSheet/index.html

-------

Linear Algebra libraries

https://java-matrix.org/

https://github.com/fommil/matrix-toolkits-java/
http://ejml.org/wiki/index.php?title=Main_Page

http://lessthanoptimal.github.io/Java-Matrix-Benchmark/runtime/2013_10_Corei7v2600/


https://github.com/fommil/netlib-java

-------


http://www.oracle.com/technetwork/java/javase/overview/javafx-overview-2158620.html

https://www.eclipse.org/efxclipse/index.html
http://wiki.eclipse.org/Efxclipse/Tooling/FXGraph


http://www.interactivemesh.org/models/jfx3dfxmlmodels.html
