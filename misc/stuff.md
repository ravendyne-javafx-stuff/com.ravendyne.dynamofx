## very general class hierarchy

Node
  - Parent
    - Group
    - Region
      - Control
        * UI controls in subclasses and subclasses of subclasses
        * also contains *Pane classes which are NOT layouts:
          - ScrollPane
          - SplitPane
          - TabPane
          
      - Pane
        * layout classes


a BuilderBase
    - build
        - buildComponent <- final subclass implements this
        - setComponentParameters <- final subclass implements this

    - buildComponent <- abstract so that BuilderBase.build() can call method of the final subclass
    - setComponentParameters <- abstract so that BuilderBase.build() can call method of the final subclass
    - setParameters
    a LayoutRegionBuilder
        - setParameters
            - super.setParameters <- BuilderBase
        a ControlLabeledBuilder
            - setParameters
                - super.setParameters <- LayoutRegionBuilder

concrete class (final)
    extends LayoutRegionBuilder or ControlLabeledBuilder or whatever...
    - setComponentParameters
        - [super.]setParameters <- has to call this if it wants other parameters set, if not, don't call it

