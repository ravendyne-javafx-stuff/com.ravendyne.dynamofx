package com.ravendyne.dynamofx.gui.builders.layout;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.RegionBuilder;

import javafx.scene.layout.BorderPane;

public final class FxBorderPaneBuilder extends RegionBuilder<BorderPane> {

    public FxBorderPaneBuilder(Map< String, CfgValue > parameters) {
        super(parameters);
    }

    public static FxBorderPaneBuilder newInstance( Map< String, CfgValue > parameters ) {
        FxBorderPaneBuilder builder = new FxBorderPaneBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        BorderPane layout = new BorderPane();

        // TODO configure alignment and margin on individual children
        // get the parameters from getMap("...", parameters) calls for each region
        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/BorderPane.html

        // here, order of calls is important when the window containing
        // BorderPane becomes smaller than th space needed for region contents.
        // in that case regions will overlap and the region added last will be
        // on top of the stack.
        layout.setBottom(createComponent(getMap(Attributes.BOTTOM, parameters)));
        layout.setTop(createComponent(getMap(Attributes.TOP, parameters)));
        layout.setLeft(createComponent(getMap(Attributes.LEFT, parameters)));
        layout.setRight(createComponent(getMap(Attributes.RIGHT, parameters)));
        layout.setCenter(createComponent(getMap(Attributes.CENTER, parameters)));
        
        rootComponent = idComponent = layout;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);
    }

}
