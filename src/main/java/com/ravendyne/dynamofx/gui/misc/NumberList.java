package com.ravendyne.dynamofx.gui.misc;

public final class NumberList extends ListOfNumbers {

    private NumberList() {
    }

    public static NumberList valueOf(String value) {
        NumberList list = new NumberList();
        
        if(value == null) {
            return null;
        }

        String[] parts = value.split(",");
        
        for(String item : parts) {
            Number number = parseNumber(item);
            if(number != null) {
                list.values.add(number);
            }
        }

        return list;
    }
}
