package com.ravendyne.dynamofx.gui.builders;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.ComponentBuilderRegistry;

import javafx.scene.control.TextInputControl;
import javafx.scene.text.Font;

//javafx.scene.layout.Region -> javafx.scene.control.Control -> javafx.scene.control.TextInputControl
/**
 * @see <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TextInputControl.html">JavaFX TextInputControl</a>
 * @param <T>
 */
public abstract class ComponentTextInputControlBuilder<T extends TextInputControl> extends ComponentBuilder<T> {

    protected ComponentTextInputControlBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    @Override
    protected void setParameters(T control) {
        super.setParameters( control );

        applyStringValue(Attributes.TEXT, parameters, (text) -> control.setText( text ) );

        applyStringValue(Attributes.PROMPT_TEXT, parameters, (prompt) -> control.setPromptText( prompt ) );

        applyBooleanValue(Attributes.EDITABLE, parameters, (editable) -> control.setEditable(editable) );
        
        applyStringValue(Attributes.FONT, parameters, (fontUrl) -> {
            // TODO add font attribute support: bold, italic, size etc.
            Font font = ComponentBuilderRegistry.INSTANCE.getResourceLoader().loadFont(fontUrl, 12);
            control.setFont(font);
        } );
    }

}
