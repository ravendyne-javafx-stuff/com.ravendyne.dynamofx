package com.ravendyne.dynamofx.gui.builders.component.textinput;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;
import com.ravendyne.dynamofx.gui.builders.ComponentTextInputControlBuilder;

import javafx.scene.control.PasswordField;

/**
 * @see <a href="https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TextField.html">JavaFX TextField</a>
 */
public final class FxPasswordFieldBuilder extends ComponentTextInputControlBuilder<PasswordField> {

    private FxPasswordFieldBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    public static FxPasswordFieldBuilder newInstance(Map<String, CfgValue> parameters) {
        FxPasswordFieldBuilder builder = new FxPasswordFieldBuilder(parameters);
        return builder;
    }

    @Override
    public void buildComponent() {

        PasswordField component = new PasswordField();

        rootComponent = idComponent = component;
    }

    @Override
    protected void setComponentParameters() {
        setParameters(rootComponent);

        applyNumericValue(Attributes.COLS, parameters, (cols) -> rootComponent.setPrefColumnCount( cols.intValue() ) );
    }

}
