package com.ravendyne.dynamofx.gui.builders;

import java.util.Map;

import com.ravendyne.dynamofx.cfg.CfgValue;
import com.ravendyne.dynamofx.gui.Attributes;

import javafx.scene.layout.Region;

//javafx.scene.layout.Region
public abstract class RegionBuilder<T extends Region> extends BuilderBase<T> {

    protected RegionBuilder(Map<String, CfgValue> parameters) {
        super(parameters);
    }

    @Override
    protected void setParameters(T region) {
        super.setParameters(region);

        applyNumericValue(Attributes.WIDTH, parameters, (width) -> region.setPrefWidth(width.doubleValue()) );

        applyNumericValue(Attributes.HEIGHT, parameters, (height) -> region.setPrefHeight(height.doubleValue()) );

        applyNumberPairValue(Attributes.SIZE, parameters, (pair) -> region.setPrefSize(pair.getX().doubleValue(), pair.getY().doubleValue()) );

        applyStringValue(Attributes.CSS_CLASS, parameters, (cssClass) -> region.getStyleClass().add(cssClass) );
        
        applyInsetsValue(Attributes.PADDING, parameters, (insets) -> region.setPadding(insets) );
        
    }

}
